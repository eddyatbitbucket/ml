# -*- coding: utf-8 -*-
import os
import sys

curPath = os.path.abspath(os.path.dirname(__file__))
project_root = os.path.dirname(curPath)
sys.path.insert(0, project_root)
# 在sys.path還未設定之前，不能執行其他import，否則可能會有module not found exception

import storage
import numpy as np
from dataset.mnist import load_mnist
from neural_network import NeuralNetwork
import storage

(x_train, t_train), (x_test, t_test) = \
    load_mnist(normalize=True, flatten=False, one_hot_label=True)
input_num = x_train.shape[1]

# layer setting
layers = storage.load()
network = NeuralNetwork(layers, iter_num=1000, batch_size=100)

# accuracy = network.accuracy(x_test, t_test)
# print(accuracy)

