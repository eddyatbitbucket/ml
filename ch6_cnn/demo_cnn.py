# -*- coding: utf-8 -*-
import os
import sys

import storage
import utility

curPath = os.path.abspath(os.path.dirname(__file__))
project_root = os.path.dirname(curPath)
sys.path.insert(0, project_root)
# 在sys.path還未設定之前，不能執行其他import，否則可能會有module not found exception


import numpy as np
from dataset.mnist import load_mnist
from neural_network import NeuralNetwork
from neural_network import Affine
from neural_network import Convolution
from neural_network import Pooling
from neural_network import Relu
from neural_network import Sigmoid
from neural_network import SoftmaxWithLoss
from neural_network import Flatten

(x_train, t_train), (x_test, t_test) = \
    load_mnist(normalize=True, flatten=False, one_hot_label=True)
input_num = x_train.shape[1]

# layer setting
conv0 = Convolution(kernel_num=30, pad=0, stride=1)
relu0 = Relu()
pool0 = Pooling(pool_h=2, pool_w=2, stride=2)

flatten = Flatten()

affine1 = Affine(100)
relu1 = Relu()

affine = Affine(10)
last_layer = SoftmaxWithLoss()

layers = [conv0, relu0, pool0,
          flatten,
          affine1, relu1,
          affine, last_layer]

network = NeuralNetwork(layers)

network.learn(x_train, t_train, plot_freq=100)

accuracy = network.accuracy(x_test, t_test)
print(accuracy)

# storage.save_layers(network.layers, 'simple_cnn.pkl')

# a2_w_imgs = (affine2.W[1].reshape(4, 4)+0.5)*255
# show_image(a2_w_imgs)
# img=x_train[0].reshape(28,28)*255
# show_image(img)
