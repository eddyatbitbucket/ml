# -*- coding: utf-8 -*-
import os
import sys
import gradient_descend_optimizer as optimizer
import utility

curPath = os.path.abspath(os.path.dirname(__file__))
project_root = os.path.dirname(curPath)
sys.path.insert(0, project_root)
print(sys.path)
# 在sys.path還未設定之前，不能執行其他import，否則可能會有module not found exception
import visualization
import storage
import numpy as np
from dataset.mnist import load_mnist
from neural_network import NeuralNetwork
from neural_network import Affine
from neural_network import Convolution3D as Convolution3D
from neural_network import Pooling3D
from neural_network import Relu
from neural_network import Sigmoid
from neural_network import SoftmaxWithLoss
from neural_network import MeanSquareLoss2 as MSE2
from neural_network import Flatten
from dataset import random_ball, lnd_dataset_with_position

# (x_train, t_train), (x_test, t_test) = random_ball.get_sample()
(x_train, t_train), (x_test, t_test) = lnd_dataset_with_position.load(include_position=True, expand_t=False, plot_while_doing=False)

# layer setting

conv1 = Convolution3D(kernel_num=10, filter_size=3, pad=1, stride=1)
relu1 = Relu()
pool1 = Pooling3D()
# conv2 = Convolution3D(kernel_num=5, filter_size=3, pad=1, stride=1)
# relu2 = Relu()
# pool2 = Pooling3D()
# conv3 = Convolution3D(kernel_num=3, filter_size=3, pad=1, stride=1)
# relu3 = Relu()
# pool3 = Pooling3D()
flatten = Flatten()

affine = Affine(4)
last_layer = MSE2()

layers = [conv1, relu1,
          pool1,
          # conv2, relu2,
          # pool2,
          # conv3, relu3,
          # pool3,

          flatten,

          affine, last_layer]

nn = NeuralNetwork(layers, optimizer=optimizer.Adam(learning_rate=0.0001))

l = storage.load('cnn_ball_localization.pkl')
if l:
    nn.layers = l

nn.learn(x_train, t_train, batch_size=100, iter_num=100, plot_freq=100, save_as='cnn_ball_localization.pkl', classification=False)

accuracy = nn.accuracy(x_test, t_test)
print(accuracy)

storage.save(nn.layers, 'cnn_ball_localization.pkl')

for i in range(20):
    Y = nn.predict(np.array([x_test[i]]))
    nd, z, y, x = Y[0]
    n, c, d, h, w = x_test.shape

    if nd > 0.5:
        xi = x_test[i]
        visualization.mip_with_point(x_test[i][0], np.array([z * d, y * h, x * w]))
