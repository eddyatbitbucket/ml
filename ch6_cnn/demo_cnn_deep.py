# -*- coding: utf-8 -*-
import os
import sys

curPath = os.path.abspath(os.path.dirname(__file__))
project_root = os.path.dirname(curPath)
sys.path.insert(0, project_root)
# 在sys.path還未設定之前，不能執行其他import，否則可能會有module not found exception

import storage
import numpy as np
from dataset.mnist import load_mnist
from neural_network import NeuralNetwork
from neural_network import Affine
from neural_network import Convolution
from neural_network import Pooling
from neural_network import Relu
from neural_network import Sigmoid
from neural_network import SoftmaxWithLoss
from neural_network import Flatten

(x_train, t_train), (x_test, t_test) = \
    load_mnist(normalize=True, flatten=False, one_hot_label=True)

# layer setting
conv0 = Convolution(kernel_num=30, filter_size=3, pad=1, stride=1)
relu0 = Relu()
pool0 = Pooling(pool_h=2, pool_w=2, stride=2)

conv1 = Convolution(kernel_num=10, filter_size=3, pad=1, stride=1)
relu1 = Relu()
pool1 = Pooling(pool_h=2, pool_w=2, stride=2)

flatten = Flatten()
affine2 = Affine(100)
relu2 = Relu()

affine = Affine(10)
last_layer = SoftmaxWithLoss()

layers = [conv0, relu0, pool0,
          conv1, relu1, pool1,
          flatten,
          affine2, relu2,
          affine, last_layer]

nn = NeuralNetwork(layers)

nn.learn(x_train, t_train)

accuracy = nn.accuracy(x_test, t_test)
print(accuracy)

storage.save(nn.layers, 'cnn_deep.pkl')

# a2_w_imgs = (affine2.W[1].reshape(4, 4)+0.5)*255
# show_image(a2_w_imgs)
# img=x_train[0].reshape(28,28)*255
# show_image(img)
