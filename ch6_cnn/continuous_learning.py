# -*- coding: utf-8 -*-
import os
import sys
import gradient_descend_optimizer as gdo

curPath = os.path.abspath(os.path.dirname(__file__))
project_root = os.path.dirname(curPath)
sys.path.insert(0, project_root)
# 在sys.path還未設定之前，不能執行其他import，否則可能會有module not found exception


from dataset.mnist import load_mnist
from neural_network import NeuralNetwork
import storage


def get_keyboard_input():
    message = """要開始學習嗎？(Y/N) >>> """
    try:
        input_text = input(message)
    except Exception as e:
        print("輸入字元有誤:", e)
        input_text = ""
    return input_text


answer = get_keyboard_input()

learn_iter_num = 1

for i in range(learn_iter_num):
    (x_train, t_train), (x_test, t_test) = \
        load_mnist(normalize=True, flatten=False, one_hot_label=True)
    input_num = x_train.shape[1]

    # layer setting
    layers = storage.load('cnn_ball_detection.pkl')
    network = NeuralNetwork(layers, optimizer=gdo.Adam(initial_iter=1000))

    if answer == "y" or answer == 'Y':
        accuracy_0 = network.accuracy(x_test, t_test)

        network.learn(x_train, t_train,
                      iter_num=10000, batch_size=100,
                      plot_freq=100)

        accuracy = network.accuracy(x_test, t_test)

        # 若是訓練後有改善才存檔
        print(f'{accuracy_0} --> {accuracy}')
        if accuracy > accuracy_0:
            storage.save(network.layers, 'cnn_ball_detection.pkl')
        else:
            print(f"iter:{i}, 訓練後，accuracy沒有改善，不存檔")
