# -*- coding: utf-8 -*-
import os
import sys
import gradient_descend_optimizer as optimizer

curPath = os.path.abspath(os.path.dirname(__file__))
project_root = os.path.dirname(curPath)
sys.path.insert(0, project_root)
# 在sys.path還未設定之前，不能執行其他import，否則可能會有module not found exception

import storage
from neural_network import NeuralNetwork
from neural_network import Affine
from neural_network import Convolution
from neural_network import Pooling
from neural_network import Relu
from neural_network import SoftmaxWithLoss
from neural_network import Flatten
from dataset import random_circle

X, T = random_circle.create_samples(10000, normalize=True)
(x_train, t_train), (x_test, t_test) = (X[:9000], T[:9000]), (X[9000:], T[9000:])

input_num = x_train.shape[1]

# layer setting
conv0 = Convolution(kernel_num=30, filter_size=3, pad=1, stride=1)
relu0 = Relu()
pool0 = Pooling(pool_h=2, pool_w=2, stride=2)

conv1 = Convolution(kernel_num=10, filter_size=3, pad=1, stride=1)
relu1 = Relu()
pool1 = Pooling(pool_h=2, pool_w=2, stride=2)

flatten = Flatten()
affine2 = Affine(100)
relu2 = Relu()

affine = Affine(2)
last_layer = SoftmaxWithLoss()

layers = [conv0, relu0, pool0,
          # conv1, relu1, pool1,
          flatten,
          # affine2, relu2,
          affine, last_layer]

nn = NeuralNetwork(layers)

nn.learn(x_train, t_train, iter_num=1000, plot_freq=100)

accuracy = nn.accuracy(x_test, t_test)
print(accuracy)

storage.save(nn.layers, 'cnn_circle_detection.pkl')

