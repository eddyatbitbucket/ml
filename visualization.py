import json
import sys, os

sys.path.append(os.pardir)

import PIL.Image
import matplotlib.pyplot as plt
import numpy as np


def show_image(image, title=''):
    pil_image = PIL.Image.fromarray(image)
    plt.title(title)
    pil_image.show()


def show_multiple_images(images, column_num):
    image_num = len(images)
    pil_images = list()

    for i in images:
        pil_images.append(PIL.Image.fromarray(i))

    row_num = int(image_num / column_num)
    residue = image_num % column_num
    if residue != 0:
        row_num += 1

    fig = plt.figure()

    for n in range(0, row_num * column_num):
        ax = fig.add_subplot(row_num, column_num, n + 1)
        ax.get_yaxis().set_visible(False)
        ax.get_xaxis().set_visible(False)
        plt.imshow(pil_images[n])
        if n >= image_num - 1:
            break

    plt.show()


def show_filter(filters, nx=8, margin=3, scale=10):
    """
    c.f. https://gist.github.com/aidiary/07d530d5e08011832b12#file-draw_weight-py
    """
    FN, C, FH, FW = filters.shape
    ny = int(np.ceil(FN / nx))

    fig = plt.figure()
    fig.subplots_adjust(left=0, right=1, bottom=0, top=1, hspace=0.05, wspace=0.05)

    for i in range(FN):
        ax = fig.add_subplot(ny, nx, i + 1, xticks=[], yticks=[])
        ax.imshow(filters[i, 0], cmap=plt.cm.get_cmap('Blues'), interpolation='nearest')
    plt.show()


def plot(*x):
    plt.plot(x)
    plt.show()


def show_3d(box, title=''):
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlim(0, box.shape[1])
    ax.set_ylim(0, box.shape[2])
    ax.set_zlim(0, box.shape[3])

    X = list()
    Y = list()
    Z = list()
    C = list()
    for x in range(box.shape[1]):
        for y in range(box.shape[2]):
            for z in range(box.shape[3]):
                color = box[0][x][y][z]
                if color > 0:
                    X.append(x)
                    Y.append(y)
                    Z.append(z)
                    C.append(color * 255)

    ax.scatter(X, Y, Z, c=C)
    # plt.plot(X,Y, 'o')
    plt.title(title)
    plt.show()


def mip(cube, axis=0):
    mip_matrix = np.max(cube, axis=axis)
    return mip_matrix


def mip_with_point(cube, point, axis=0):
    """

    :param cube: ndarray: [z,y,x]
    :param point: ndarray: [z,y,x]
    :param axis:
    :return:
    """
    mip_cube = mip(cube, axis=axis)

    point = np.rint(point)
    p = point.astype(int)
    p = np.clip(p, a_min=[0, 0, 0], a_max=[6, 31, 31])

    mask = np.ones(p.shape, dtype=bool)
    mask[axis] = False
    p = p[mask]

    mip_cube[p[0] - 1:p[0] + 2][p[1] - 1:p[1] + 2] = 32
    mip_cube[p[0]][p[1]] = 0

    plt.imshow(mip_cube, cmap='gray')
    plt.show()


def show_3d_multiple(boxes, limit=10):
    n = len(boxes)

    start = n - (min(limit, n)) if limit else 0

    for i in range(start, n):
        show_3d(boxes[i])


import torch


# %%
def pytorch_imshow(im, title=None):
    im = im.numpy().transpose((1, 2, 0))
    m = np.array(mean)
    s = np.array(sd)
    im = s * im + mean
    im = np.clip(im, 0, 1)
    plt.imshow(im)
    if title is not None:
        plt.title(title)
    plt.show()


# %%
def pytorch_visualize_model(model, dataloaders, class_names, num_images=6):
    is_cuda = next(model.parameters()).is_cuda
    device = torch.device('cuda' if is_cuda else 'cpu')

    was_training = model.training
    model.eval()
    images_so_far = 0

    with torch.no_grad():
        for i, (inputs, labels) in enumerate(dataloaders['val']):
            inputs = inputs.to(device)
            labels = labels.to(device)

            outputs = model(inputs)
            _, preds = torch.max(outputs, 1)

            for j in range(inputs.size()[0]):
                images_so_far += 1
                plt.title('predicted: {}'.format(class_names[preds[j]]))
                pytorch_imshow(inputs.cpu().data[j])

                if images_so_far == num_images:
                    model.train(mode=was_training)
                    return

        model.train(mode=was_training)


class LayerManager:
    def __init__(self, layers=[]):
        self.layers = layers

    def insert_layer(self, layer, index=0):
        self.layers.insert(index, layer)

    def output_img(self):
        reversed_layers = self.layers[::-1]
        img = reversed_layers[0].img.copy()
        color_channel = 2
        for l in reversed_layers[1:]:
            alpha_mask = l.img[:, :, color_channel] * l.alpha

            img[:, :, color_channel] = img[:, :, color_channel] * (np.ones(alpha_mask.shape) - alpha_mask) \
                                       + l.img[:, :, color_channel] * alpha_mask
        return img

    def __repr__(self):
        return json.dumps([(l.name, l.img.shape) for l in self.layers])


class Layer:
    def __init__(self, name, img, alpha=1.0):
        self.name = name
        self.img = (img - np.min(img)) / np.ptp(img)
        self.alpha = np.clip([alpha], a_min=0.0, a_max=1.0)[0]

    def __repr__(self):
        return json.dumps(self.name)


def get_heatmap(fg_img, bg_img, rgba_channel=[0, 0, 1, 1], bg_as_grayscale=True):
    alpha = rgba_channel[3]
    if bg_as_grayscale:
        grayscale = (bg_img[:, :, 0] + bg_img[:, :, 1] + bg_img[:, :, 2]) / 3
        bg_img[:, :, 0], bg_img[:, :, 1], bg_img[:, :, 2] = grayscale, grayscale, grayscale

    heatmap = bg_img
    for c in range(3):
        channel_value = fg_img[:, :, c] * rgba_channel[c] * alpha
        heatmap[:, :, c] = heatmap[:, :, c] * (1 - channel_value) + \
                           fg_img[:, :, c] * channel_value
    return heatmap


if __name__ == '__main__':
    im = np.array([
        [[21, 100, 100],
         [25, 23, 30],
         [100, 100, 100]],
        [[22, 100, 100],
         [100, 100, 100],
         [100, 100, 0]],
        [[34, 100, 100],
         [100, 0, 0],
         [100, 100, 0]],
    ])
    im2 = np.array([
        [[100, 100, 100],
         [100, 100, 100],
         [100, 100, 100]],
        [[22, 100, 100],
         [100, 100, 100],
         [100, 100, 100]],
        [[34, 100, 100],
         [255, 0, 0],
         [255, 0, 0]],
    ])
    # mip_with_point(im, np.array((0, 0, 0)), axis=2)

    # %% layer manager
    layer1 = Layer('layer1', im, alpha=0.5)
    layer2 = Layer('layer2', im2, alpha=1.0)
    lm = LayerManager([layer1, layer2])
    print(lm.output_img())
    blended_img = lm.output_img()
    plt.imshow(blended_img)
    plt.show()
