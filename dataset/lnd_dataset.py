import storage
import visualization
import utility
import numpy as np
import random
import json
import matplotlib.pyplot as plt


def load(use_cache=False, plot_while_doing=False):
    x_t = None
    file_path = '../dataset/lnd_x_t.pkl'

    if use_cache:
        x_t = storage.load(file_path)

    if not x_t:
        print('從csv中取出資料...')
        dataset = utility.read_csv('../dataset/dataset.csv')
        random.shuffle(dataset)

        X = []
        T = []
        nodule_position = []

        for d in dataset:
            X.append(json.loads(d[0]))
            T.append(int(d[1]))
            nodule_position.append(json.loads(d[2]))

        size = len(dataset)
        max_test_num = 300
        train_size = size * 10 // 11
        if (size - train_size) > max_test_num:
            train_size = size - max_test_num

        X = np.array(X)
        T = np.array(T)

        TT = np.zeros((T.shape[0], 2))
        TT[:, 0] = T
        TT[:, 1] = 1 - T

        XX = np.expand_dims(X, 1)

        x_train = XX[:train_size]
        t_train = TT[:train_size]

        x_test = XX[train_size:]
        t_test = TT[train_size:]

        x_t = ((x_train, t_train), (x_test, t_test))
        storage.save(x_t, file_path, compress=True)

        if plot_while_doing:
            for i in range(X.shape[0]):
                mip = XX[i].max(axis=1)
                img = mip.reshape(mip.shape[1], mip.shape[2])
                fig, ax = plt.subplots(nrows=1, ncols=1)
                if T[i]:
                    fig.patch.set_facecolor('xkcd:mint green')
                ax.imshow(img, cmap='gray')
                plt.show()

    return x_t


if __name__ == '__main__':
    data = load(plot_while_doing=False)
