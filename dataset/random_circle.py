from PIL import Image, ImageDraw

import storage
import visualization
from imageio import imread
import numpy as np

img_size = (28, 28)
img_type = "GIF",
mode = "RGB",
bg_color = (255, 255, 255),
fg_color = (0, 0, 255),
font_size = 18,
font_type = "Monaco.ttf",  # 字體網上隨意找,有些時候數字不能顯示就是字體不對,換一種即可
length = 4,
draw_lines = True,
# n_line = (1, 2),
draw_points = True,
point_chance = 2


def create_lines(draw):
    """繪製干擾線"""
    line_num = np.random.randint(1, 4)  # 干擾線條數

    for i in range(line_num):
        # 起始點
        begin = (np.random.randint(0, img_size[0]), np.random.randint(0, img_size[1]))
        # 結束點
        end = (np.random.randint(0, img_size[0]), np.random.randint(0, img_size[1]))
        hu = np.random.randint(200, 255)
        width = np.random.randint(1, 4)
        draw.line([begin, end], width=width, fill=hu)


# def create_points():
#     """繪製干擾點"""
#     chance = min(100, max(0, int(point_chance)))  # 大小限制在[0, 100]
#
#     for w in range(img_size[0]):
#         for h in range(img_size[1]):
#             tmp = np.random.randint(0, 100)
#             if tmp > 100 - chance:
#                 draw.point((w, h), fill=(0, 0, 0))


def create_samples(number, normalize=True):
    path = './random_circle/'

    n = number
    diameter = np.random.randint(7, int(min(img_size) / 2), n)
    has_circle = np.random.randint(0, 100, n) > 70
    has_line = np.random.randint(0, 100, n) > 10

    hu = np.random.randint(200, 255, n)
    X = list()
    T = list()

    for i, d in enumerate(diameter):
        image = Image.new('L', img_size)
        draw = ImageDraw.Draw(image)

        if has_line[i]:
            create_lines(draw)

        if has_circle[i]:
            x = np.random.randint(0, int(img_size[0] / 2), 1)
            y = np.random.randint(0, int(img_size[1] / 2), 1)
            gray_scale = tuple([hu[i]])
            draw.ellipse((x, y, x + d, y + d), fill=gray_scale)

        X.append(np.asarray(image))
        T.append(np.array([has_circle[i], 1 - has_circle[i]]))

        # file = f'test{i}.png'
        # full_url = f'{path}{file}'
        # image.save(full_url)

    X, T = np.array(X), np.array(T)
    X = X.reshape(X.shape[0], 1, 28, 28)
    T = T.reshape(T.shape[0], 2)
    visualization.show_multiple_images(X.reshape(X.shape[0], 28, 28)[:10], 5)

    if normalize:
        X = X / 255.0

    return X, T


file = 'circle_sample.pkl'


def load_sample():
    storage.load(file)


if __name__ == '__main__':
    print('creating samples...')
    X, T = create_samples(1000)
    storage.save((X, T), file)
