# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
import os
import sys
from pathlib import Path

home = str(Path.home())
sys.path.insert(0, os.path.join(home, 'PycharmProjects/ml'))
import utility
import storage
import visualization

import numpy as np

file = 'random_ball.pkl'

box_size = (32, 32, 32)
img_type = "GIF",
length = 4,
draw_lines = True,


# n_line = (1, 2),


def create_lines(box):
    """繪製干擾線"""
    line_num = np.random.randint(1, 3)  # 干擾線條數

    for i in range(line_num):
        # 起始點
        begin = (np.random.randint(0, box_size[0]),
                 np.random.randint(0, box_size[1]),
                 np.random.randint(0, box_size[2]))
        # 結束點
        end = (np.random.randint(0, box_size[0]),
               np.random.randint(0, box_size[1]),
               np.random.randint(0, box_size[2]))

        # 線寬
        width = np.random.randint(3, 6)

        # 顏色
        hu = np.random.randint(40, 80)

        # 處理起始點與終點
        x0, y0, z0 = begin[0], begin[1], begin[2]
        x1, y1, z1 = end[0], end[1], end[2]

        if x0 > x1:
            x0, x1 = x1, x0
        if x0 == x1:
            x0 -= width
            x1 += width

        if y0 > y1:
            y0, y1 = y1, y0
        if y0 == y1:
            y0 -= width
            y1 += width

        if z0 > z1:
            z0, z1 = z1, z0
        if z0 == z1:
            z0 -= width
            z1 += width

        v = np.array([x1 - x0,
                      y1 - y0,
                      z1 - z0])

        max_v = np.argmax(v)

        if max_v == 0:
            for x in range(x0, x1):
                dx = x - x0
                dy = (y1 - y0) / (x1 - x0) * dx
                dz = (z1 - z0) / (x1 - x0) * dx
                box = draw_ball(box, x, y0 + dy, z0 + dz, round(width / 2), hu)
        elif max_v == 1:
            for y in range(y0, y1):
                dy = y - y0
                dx = (x1 - x0) / (y1 - y0) * dy
                dz = (z1 - z0) / (y1 - y0) * dy
                box = draw_ball(box, x0 + dx, y, z0 + dz, round(width / 2), hu)
        elif max_v == 2:
            for z in range(z0, z1):
                dz = z - z0
                dx = (x1 - x0) / (z1 - z0) * dz
                dy = (y1 - y0) / (z1 - z0) * dz
                box = draw_ball(box, x0 + dx, y0 + dy, z, round(width / 2), hu)

    return box


# def draw_ball(box, x0, y0, z0, r, fill):
#     for x in range(box.shape[0]):
#         for y in range(box.shape[1]):
#             for z in range(box.shape[2]):
#                 micro_lobulation = np.random.randint(0, 2, 1)
#                 if micro_lobulation:
#                     if (x - x0) ** 2 + (y - y0) ** 2 + (z - z0) ** 2 < ((r * 0.9) ** 2):
#                         box[x][y][z] = fill
#                 else:
#                     if (x - x0) ** 2 + (y - y0) ** 2 + (z - z0) ** 2 <= r ** 2:
#                         box[x][y][z] = fill
#     return box


def draw_ball(box, x0, y0, z0, r, fill):
    xi, yi, zi = np.indices(box.shape)

    r = np.random.randn(box.shape[0], box.shape[1], box.shape[2]) * 0.1 + r
    mask = (xi - x0) ** 2 + (yi - y0) ** 2 + (zi - z0) ** 2 <= r ** 2
    box[mask] = fill

    return box


def create_samples(number, normalize=True, save=False):
    print('creating sample...')

    X = list()
    T = list()

    n = number
    diameter = np.random.randint(7, int(min(box_size) / 2), n)
    has_ball = np.random.randint(0, 100, n) > 70
    has_line = np.random.randint(0, 100, n) > 10

    hu = np.random.randint(40, 80, n)

    t0 = utility.Time.get_epoch_time_second()
    for i, d in enumerate(diameter):

        box = np.zeros(box_size)

        if has_line[i]:
            create_lines(box)

        if has_ball[i]:
            r = d / 2
            rr = round(r)
            x = np.random.randint(rr, int(box_size[0] - rr), 1)
            y = np.random.randint(rr, int(box_size[1] - rr), 1)
            z = np.random.randint(rr, int(box_size[2] - rr), 1)
            gray_scale = hu[i]
            draw_ball(box, x, y, z, r, fill=gray_scale)

        X.append(np.asarray(box))
        T.append(np.array([has_ball[i], 1 - has_ball[i]]))

        # 計算還需多久完成
        t1 = utility.Time.get_epoch_time_second()
        iter_rate = (i + 1) / (t1 - t0)
        remaining_time = (n - i - 1) / iter_rate
        print(f'{i+1}/{n}, {round(remaining_time/60,1)} min remaining')

    X, T = np.array(X), np.array(T)
    X = X.reshape(X.shape[0], 1, box.shape[0], box.shape[1], box.shape[2])
    T = T.reshape(T.shape[0], 2)

    if normalize:
        X = X / 255.0

    print('sample created')

    if save:
        # 若是有舊的data, 則附加上去
        print('loading old data...')
        data = storage.load(file)

        print('appending new data...')
        if data:
            X0 = data[0]
            T0 = data[1]
            X = np.concatenate((X0, X), axis=0)
            T = np.concatenate((T0, T), axis=0)

        storage.save((X, T), file)
        print(f'saved: {file}')

    return X, T


def get_sample():
    try:
        X, T = storage.load(file)
    except:
        print('no preexisting sample.')
        X, T = create_samples(1000, normalize=True, save=True)
    ratio = 0.1
    max_test_size = 1000
    test_size = min(int(X.shape[0] * ratio), max_test_size)
    train_size = X.shape[0] - test_size
    result = (X[:train_size], T[:train_size]), (X[train_size:], T[train_size:])
    return result


if __name__ == '__main__':
    print('creating samples...')
    iter_num = 10
    batch = 10

    t0 = utility.Time.get_epoch_time_second()

    for i in range(iter_num):
        X, T = create_samples(batch, save=True)
        print(X.shape, T.shape)
        visualization.show_3d_multiple(X, 10)
        # visualization.show_3d(X[-1])

        # 計算還需多久完成
        t1 = utility.Time.get_epoch_time_second()
        iter_rate = (i + 1) / (t1 - t0)
        remaining_time = (iter_num - i - 1) / iter_rate
        utility.log_progress((i + 1) / iter_num, f'iter: {i+1}/{iter_num}, {round(remaining_time/60,1)} min remaining')
