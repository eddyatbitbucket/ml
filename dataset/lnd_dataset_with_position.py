import storage
import visualization
import utility
import numpy as np
import random
import json
import matplotlib.pyplot as plt


def load(include_position=False, expand_t=True, use_cache=True, plot_while_doing=False):
    dataset = None
    file_path = '../dataset/lnd_dataset.pkl'

    if use_cache:
        dataset = storage.load(file_path)

    if not dataset:
        dataset = utility.read_csv('../dataset/dataset.csv')
        random.shuffle(dataset)
        storage.save(dataset, file_path)

    X = []
    T = []
    nodule_position = []

    for d in dataset:
        X.append(json.loads(d[0]))
        T.append(int(d[1]))
        nodule_position.append(json.loads(d[2]))

    size = len(dataset)
    max_test_num = 300
    train_size = size * 10 // 11
    if (size - train_size) > max_test_num:
        train_size = size - max_test_num

    X = np.array(X)
    T = np.array(T)
    NP = np.array(nodule_position)
    np_mask = (NP[:, 0] == -1)

    NP[np_mask] = 0
    n, d, h, w = X.shape
    NP = NP / np.array([d, h, w])

    if expand_t:
        TT = np.zeros((T.shape[0], 2))
        TT[:, 0] = T
        TT[:, 1] = 1 - T
    else:
        TT = T

    XX = np.expand_dims(X, 1)

    x_train = XX[: train_size]
    if include_position:
        if expand_t:
            t_train = np.concatenate((TT[: train_size], NP[:train_size]), axis=1)
        else:
            t_train = np.concatenate((np.array([TT[: train_size]]).T, NP[:train_size]), axis=1)
    else:
        t_train = TT[:train_size]

    x_test = XX[train_size:]
    if include_position:
        if expand_t:
            t_test = np.concatenate((TT[train_size:], NP[train_size:]), axis=1)
        else:
            t_test = np.concatenate((np.array([TT[train_size:]]).T, NP[train_size:]), axis=1)
    else:
        t_test = TT[train_size:]

    if plot_while_doing:
        range_max = 20
        range_num = X.shape[0] if range_max > X.shape[0] else range_max
        for i in range(range_num):
            mip = XX[i].max(axis=1)
            img = mip.reshape(mip.shape[1], mip.shape[2])
            fig, ax = plt.subplots(nrows=1, ncols=1)
            if T[i]:
                fig.patch.set_facecolor('xkcd:mint green')
            ax.imshow(img, cmap='gray')
            plt.show()

    return (x_train, t_train), (x_test, t_test)


if __name__ == '__main__':
    data = load(expand_t=False, include_position=True, plot_while_doing=True)
