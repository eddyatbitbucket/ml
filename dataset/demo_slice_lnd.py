import pickle
import matplotlib.pyplot as plt

with open("./dataset/slice_pickle/3700", "rb") as f:
    demo = pickle.load(f)
for i in range(10):
    plt.imshow(demo[:, :, i], cmap='gray')
    plt.show()
