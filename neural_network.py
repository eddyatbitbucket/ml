import numpy as np
import activation_function
import loss_function
import gradient_descent as gd
import statistics
import storage
import utility
from tensor import im2col, col2im
from tensor import Im2col3D
import optimizer as gdo
import uuid


class NeuralNetwork:
    def __init__(self, layers, optimizer=gdo.Adam()):
        self.layers = layers
        self.optimizer = optimizer
        self.learning_rate = optimizer.lr
        self.accuracy_history = list()
        self.loss_history = list()
        self.loss = None
        self.X = None
        self.T = None

    def predict(self, X):
        for layer in self.layers[:-1]:
            X = layer.forward(X)

        # 不需要最後一層的softmax, 只需要算至倒數第二層就可以
        return X

    def forward_all(self, X, T):
        for layer in self.layers[:-1]:
            if layer.type == Layer.DROPOUT:
                X = layer.forward(X, train_flag=True)
            else:
                X = layer.forward(X)
        last_layer = self.layers[-1]
        Loss, Y = last_layer.forward(X, T)
        self.loss = Loss
        return Loss, Y

    def get_loss(self):
        if not self.loss:
            loss, Y = self.forward_all(self.X, self.T)
            self.loss = loss
        return self.loss

    def accuracy(self, X, T):
        N = X.shape[0]
        batch_size = min(N, 100)
        x_split = np.array_split(X, batch_size)
        t_split = np.array_split(T, batch_size)
        n = 0
        correct_num = 0
        for i in range(len(x_split)):
            Y_max_index = np.argmax(self.predict(x_split[i]), axis=1)
            T_max_index = np.argmax(t_split[i], axis=1)

            correct_num += np.sum(Y_max_index == T_max_index)

        accuracy = correct_num / N

        return accuracy

    def numerical_gradient_descent(self, X, T):
        learning_rate = 0.01

        def loss_W():
            return self.get_loss()

        self.X = X
        self.T = T

        for layer in self.layers:
            if layer.type == Layer.AFFINE:
                layer.dW = learning_rate * gd.numerical_gradient(loss_W, layer.W)
                layer.dB = learning_rate * gd.numerical_gradient(loss_W, layer.B)

        for layer in self.layers:
            if layer.type == Layer.AFFINE:
                layer.W -= layer.dW
                layer.B -= layer.dB

    def bp_gradient_descent(self, X, T):
        self.X = X
        self.T = T

        # forward
        Loss, Y = self.forward_all(X, T)
        self.loss = Loss

        # backward
        dout = 1
        reversed_layers = self.layers[::-1]

        for l in reversed_layers:
            dout = l.backward(dout)
            if l.type in Layer.layer_with_weight and l.mutable:
                self.optimizer.update(l)

    def learn(self, X, T, batch_size=100, iter_num=100,
              plot_freq=100, save_as='', classification=True):

        t0 = utility.Time.get_epoch_time_second()
        train_size = X.shape[0]

        for i in range(iter_num):
            # 準備訓練資料
            batch_mask = np.random.choice(train_size, batch_size)
            x_batch = X[batch_mask]
            t_batch = T[batch_mask]
            self.X = x_batch
            self.T = t_batch

            if i == 0:
                self.loss_history.append(self.get_loss())
                if classification:
                    self.accuracy_history.append(self.accuracy(x_batch, t_batch))

            # 訓練：梯度下降
            self.bp_gradient_descent(x_batch, t_batch)
            # self.numerical_gradient_descent(x_batch, t_batch)

            # 繪圖
            if plot_freq and not (i + 1) % plot_freq:
                loss = self.get_loss()
                self.loss_history.append(loss)
                if plot_freq:
                    if classification:
                        self.accuracy_history.append(self.accuracy(x_batch, t_batch))
                        statistics.plot_all(self.loss_history, self.accuracy_history)
                    else:

                        statistics.plot_loss(self.loss_history)

                if save_as:
                    storage.save(self.layers, save_as)

            # 計時
            t1 = utility.Time.get_epoch_time_second()
            iter_rate = (i + 1) / (t1 - t0)
            remaining_time = (iter_num - i) / iter_rate / 60
            utility.log_progress(i / iter_num, f'loss: {self.get_loss()}', f'{i}/{iter_num}, train size: {train_size}, batch_size: {batch_size}, {round(remaining_time,1)} min remained')

        if classification:
            statistics.plot_all(self.loss_history, self.accuracy_history)
        else:
            statistics.plot_loss(self.loss_history)


class Layer:
    RELU, AFFINE, SIGMOID, CONVOLUTION, POOLING, DROPOUT, FLATTEN, INFLATE, TANH, \
    MEAN_SQUARE_LOSS, SOFTMAX_WITH_LOSS = \
        1, 2, 3, 4, 5, 6, 7, 8, 9, 998, 999
    layer_with_weight = {AFFINE, CONVOLUTION}

    def __init__(self):
        self.id = uuid.uuid4()
        self.type = None
        self.input_num = 0
        self.neuron_num = 0
        self.X = None
        self.Y = None
        self.mutable = True


class Relu(Layer):

    def __init__(self):
        super().__init__()
        self.type = Layer.RELU
        self.mask = None

    def forward(self, X):
        # y=x if x>0
        # y=0 if x<=0

        self.mask = (X <= 0)
        out = X.copy()
        out[self.mask] = 0

        return out

    def backward(self, dout):
        # dy/dx=1 if x>0
        # dy/dx=0 if x<=0

        dout[self.mask] = 0

        return dout


class Tanh(Layer):

    def __init__(self):
        super().__init__()
        self.type = Layer.TANH

    def forward(self, X):
        out = activation_function.tanh(X)

        return out

    def backward(self, dout):
        # dtan(x)/dx = 1 - tanh(x)^2

        dout = 1 - activation_function.tanh(dout) ** 2

        return dout


class Sigmoid(Layer):

    def __init__(self):
        super().__init__()
        self.type = Layer.SIGMOID
        self.out = None

    def forward(self, X):
        self.out = 1 / (1 + np.exp(-X))
        return self.out

    def backward(self, dout):
        return dout * self.out * (1 - self.out)


class Affine(Layer):

    def __init__(self, neuron_num):
        super().__init__()
        self.type = Layer.AFFINE
        self.W = None
        self.dW = None
        self.B = None
        self.dB = None
        self.neuron_num = neuron_num
        self.original_x_shape = None
        self.weight_initialized = False

    def init_weight(self, input_num, neuron_num):
        # 初始化權重，He default，使用1/sqrt(input_num/2)標準差的常態分佈
        self.W = np.random.randn(input_num, self.neuron_num) / np.sqrt(input_num / 2)
        self.B = np.random.randn(neuron_num) / np.sqrt(input_num / 2)
        self.weight_initialized = True

    def forward(self, X):
        if not self.weight_initialized:
            self.init_weight(X.shape[1], self.neuron_num)

        # y=wx+b
        # self.original_x_shape = X.shape
        # X = X.reshape(X.shape[0], -1)
        self.X = X

        return np.dot(self.X, self.W) + self.B

    def backward(self, dout):
        # dy/dw=x
        # dy/db=1
        self.dW = np.dot(self.X.T, dout)
        self.dB = np.sum(dout, axis=0)
        dout_prev = np.dot(dout, self.W.T)
        # dout_prev = dout_prev.reshape(self.original_x_shape)
        return dout_prev


class Flatten(Layer):

    def __init__(self):
        super().__init__()
        self.type = Layer.FLATTEN

        self.original_x_shape = None

    def forward(self, X):
        self.original_x_shape = X.shape
        X = X.reshape(X.shape[0], -1)

        return X

    def backward(self, dout):
        return dout.reshape(self.original_x_shape)


class Inflate(Layer):

    def __init__(self, shape):
        super().__init__()
        self.type = Layer.INFLATE

        self.original_x_shape = None
        self.inflate_x_shape = shape

    def forward(self, X):
        self.original_x_shape = X.shape
        X = X.reshape(self.inflate_x_shape)

        return X

    def backward(self, dout):
        return dout.reshape(self.original_x_shape)


class Convolution(Layer):

    def __init__(self, kernel_num, filter_size=3, stride=1, pad=1):
        super().__init__()
        self.type = Layer.CONVOLUTION
        self.kernel_num = kernel_num
        self.filter_size = filter_size
        self.stride = stride
        self.pad = pad

        self.col = None
        self.col_W = None

        self.W = None
        self.B = None
        self.dW = None
        self.dB = None
        self.weight_initialized = False

    def init_weight(self, input_num, kernel_num, filter_size):
        weight_init_scale = 0.01
        self.W = weight_init_scale * \
                 np.random.randn(kernel_num, input_num,
                                 filter_size, filter_size)
        self.B = np.zeros(self.kernel_num)
        self.weight_initialized = True

    def forward(self, X):
        if not self.weight_initialized:
            self.init_weight(X.shape[1],
                             self.kernel_num,
                             self.filter_size)

        FN, C, FH, FW = self.W.shape
        N, C, H, W = X.shape
        out_h = 1 + int((H + 2 * self.pad - FH) / self.stride)
        out_w = 1 + int((W + 2 * self.pad - FW) / self.stride)

        col = im2col(X, FH, FW, self.stride, self.pad)
        col_W = self.W.reshape(FN, -1).T

        out = np.dot(col, col_W) + self.B
        out = out.reshape(N, out_h, out_w, -1).transpose(0, 3, 1, 2)

        self.X = X
        self.col = col
        self.col_W = col_W
        return out

    def backward(self, dout):
        FN, C, FH, FW = self.W.shape
        dout = dout.transpose(0, 2, 3, 1).reshape(-1, FN)

        self.dB = np.sum(dout, axis=0)
        self.dW = np.dot(self.col.T, dout)
        self.dW = self.dW.transpose(1, 0).reshape(FN, C, FH, FW)

        dcol = np.dot(dout, self.col_W.T)
        dout_prev = col2im(dcol, self.X.shape, FH, FW, self.stride, self.pad)

        return dout_prev


class Convolution3D(Layer):

    def __init__(self, kernel_num, filter_size=3, stride=1, pad=1):
        super().__init__()
        self.type = Layer.CONVOLUTION
        self.kernel_num = kernel_num
        self.filter_size = filter_size
        self.stride = stride
        self.pad = pad
        self.index_cache = None
        self.col_X = None
        self.col_W = None

        self.W = None
        self.B = None
        self.dW = None
        self.dB = None
        self.weight_initialized = False
        self.input_shape = None

    def init_weight(self, input_num, kernel_num, filter_size):
        weight_init_scale = 0.01
        self.W = weight_init_scale * \
                 np.random.randn(kernel_num, input_num,
                                 filter_size, filter_size, filter_size)
        self.B = np.zeros(self.kernel_num)
        self.weight_initialized = True

    def forward(self, X):
        if not self.weight_initialized:
            self.init_weight(X.shape[1],
                             self.kernel_num,
                             self.filter_size)

        FN, C, FD, FH, FW = self.W.shape
        N, C, D, H, W = X.shape
        out_d = int((D + 2 * self.pad - FD) / self.stride) + 1
        out_h = int((H + 2 * self.pad - FH) / self.stride) + 1
        out_w = int((W + 2 * self.pad - FW) / self.stride) + 1

        # 降維
        self.input_shape = X.shape
        col_X, self.index_cache = Im2col3D.compress(X, FD, FH, FW, self.stride, self.pad,
                                                    use_cache=False)
        col_W = self.W.reshape(FN, -1).T

        # 計算輸出
        out = np.dot(col_X, col_W) + self.B

        # 還原維度
        out = out.reshape(N, out_d, out_h, out_w, -1).transpose(0, 4, 1, 2, 3)

        # self.X = X
        self.col_X = col_X
        self.col_W = col_W
        return out

    def backward(self, dout):
        FN, C, FD, FH, FW = self.W.shape
        dout = dout.transpose(0, 2, 3, 4, 1).reshape(-1, FN)

        self.dB = np.sum(dout, axis=0)
        self.dW = np.dot(self.col_X.T, dout).transpose(1, 0).reshape(FN, C, FD, FH, FW)

        dcol = np.dot(dout, self.col_W.T)
        dout_prev = Im2col3D.restore(self.input_shape, dcol, FD, FH, FW, self.stride, self.pad)

        return dout_prev


class Pooling3D(Layer):

    def __init__(self, pool_d=2, pool_h=2, pool_w=2, stride=1, pad=0):
        super().__init__()
        self.type = Layer.POOLING
        self.pool_d = pool_d
        self.pool_h = pool_h
        self.pool_w = pool_w
        self.stride = stride
        self.pad = pad
        self.arg_max = None
        self.input_shape = None
        self.im2col_index_cache = None

    def forward(self, X):
        self.input_shape = X.shape
        N, C, D, H, W = X.shape
        out_d = int(1 + (D - self.pool_d) / self.stride)
        out_h = int(1 + (H - self.pool_h) / self.stride)
        out_w = int(1 + (W - self.pool_w) / self.stride)

        # 降維
        col_pool, self.im2col_index_cache = Im2col3D.compress(X, self.pool_d, self.pool_h, self.pool_w,
                                                              self.stride, self.pad,
                                                              use_cache=False)
        col_pool = col_pool.reshape(-1, self.pool_d * self.pool_h * self.pool_w)

        arg_max = np.argmax(col_pool, axis=1)
        out = np.max(col_pool, axis=1)
        out = out.reshape(N, out_d, out_h, out_w, C).transpose(0, 4, 1, 2, 3)

        self.X = X
        self.arg_max = arg_max

        return out

    def backward(self, dout):
        dout = dout.transpose(0, 2, 3, 4, 1)

        pool_size = self.pool_d * self.pool_h * self.pool_w
        dmax = np.zeros((dout.size, pool_size))
        dmax[np.arange(self.arg_max.size), self.arg_max.flatten()] = dout.flatten()
        dmax = dmax.reshape(dout.shape + (pool_size,))

        dcol = dmax.reshape(dmax.shape[0] *
                            dmax.shape[1] *
                            dmax.shape[2] *
                            dmax.shape[3],
                            -1)
        dout_prev = Im2col3D.restore(self.input_shape, dcol, self.pool_d, self.pool_h, self.pool_w,
                                     self.stride, self.pad)

        return dout_prev


class Pooling(Layer):

    def __init__(self, pool_h, pool_w, stride=1, pad=0):
        super().__init__()
        self.type = Layer.POOLING
        self.pool_h = pool_h
        self.pool_w = pool_w
        self.stride = stride
        self.pad = pad
        self.arg_max = None

    def forward(self, X):
        N, C, H, W = X.shape
        out_h = int(1 + (H - self.pool_h) / self.stride)
        out_w = int(1 + (W - self.pool_w) / self.stride)

        col = im2col(X, self.pool_h, self.pool_w, self.stride, self.pad)
        col = col.reshape(-1, self.pool_h * self.pool_w)

        arg_max = np.argmax(col, axis=1)
        out = np.max(col, axis=1)
        out = out.reshape(N, out_h, out_w, C).transpose(0, 3, 1, 2)

        self.X = X
        self.arg_max = arg_max

        return out

    def backward(self, dout):
        dout = dout.transpose(0, 2, 3, 1)

        pool_size = self.pool_h * self.pool_w
        dmax = np.zeros((dout.size, pool_size))
        dmax[np.arange(self.arg_max.size), self.arg_max.flatten()] = \
            dout.flatten()
        dmax = dmax.reshape(dout.shape + (pool_size,))

        dcol = dmax.reshape(dmax.shape[0] *
                            dmax.shape[1] *
                            dmax.shape[2],
                            -1)
        dout_prev = col2im(dcol, self.X.shape, self.pool_h, self.pool_w,
                           self.stride, self.pad)

        return dout_prev


class Dropout(Layer):

    def __init__(self, dropout_ratio=0.1):
        super().__init__()
        self.type = Layer.DROPOUT
        self.dropout_ratio = dropout_ratio
        self.mask = None

    def forward(self, X, train_flag=False):
        if train_flag:
            self.mask = np.random.rand(*X.shape) > self.dropout_ratio
            out = X * self.mask
        else:
            out = X * (1.0 - self.dropout_ratio)

        return out

    def backward(self, dout):

        return dout * self.mask


class SoftmaxWithLoss(Layer):
    def __init__(self):
        super().__init__()
        self.type = Layer.SOFTMAX_WITH_LOSS
        self.T = None
        self.loss = None

    def forward(self, X, T=None):

        self.X = X
        self.Y = activation_function.softmax(X)
        if T is not None:
            self.T = T
            self.loss = loss_function.cross_entropy(self.Y, self.T)
            return self.loss, self.Y
        else:
            return self.Y

    def backward(self, dout=1):
        batch_size = self.T.shape[0]

        return dout * (self.Y - self.T) / batch_size


class MeanSquareLoss(Layer):
    def __init__(self):
        super().__init__()
        self.type = Layer.MEAN_SQUARE_LOSS
        self.T = None
        self.loss = None

    def forward(self, X, T):
        self.T = T
        self.X = X
        self.Y = X

        self.loss = loss_function.mean_square_error(self.X, self.T)
        return self.loss, self.Y

    def backward(self, dout=1):
        batch_size = self.T.shape[0]

        return dout * 2 * (self.Y - self.T) / batch_size


class MeanSquareLoss2(Layer):
    def __init__(self):
        super().__init__()
        self.type = Layer.MEAN_SQUARE_LOSS
        self.T = None
        self.loss = None

    def loss_function(self, Y, T):
        batch_size = Y.shape[0]
        loss = 0
        for i in range(batch_size):
            if T[i][0] == 1:
                loss += sum((T[i] - Y[i]) ** 2)
            else:
                loss += (T[i][0] - Y[i][0]) ** 2
        loss = loss / batch_size
        return loss

    def forward(self, X, T):
        self.T = T
        self.X = X
        self.Y = X

        # mask = (self.T[:, 0] == 1)
        # num_mask = mask.sum()
        # reversed_mask = (self.T[:, 0] == 0)
        # num_reversed_mask = reversed_mask.sum()
        #
        # loss_t_eq_1 = loss_function.mean_square_error(self.Y[mask], self.T[mask]) * num_mask
        # loss_t_eq_0 = loss_function.mean_square_error(self.Y[reversed_mask][0], self.T[reversed_mask][0]) * num_reversed_mask
        # self.loss = (loss_t_eq_1 + loss_t_eq_0) / (num_mask + num_reversed_mask)

        self.loss = self.loss_function(self.Y, self.T)
        return self.loss, self.Y

    def backward(self, dout=1):
        batch_size = self.T.shape[0]
        d_out_all = np.zeros(self.T.shape)
        for i in range(batch_size):
            if self.T[i][0] == 1:
                d_out_all[i] = (dout * 2 * (self.Y[i] - self.T[i]))
            else:
                d_out_all[i][0] = (dout * 2 * (self.Y[i][0] - self.T[i][0]))

        d_out_all = d_out_all / batch_size

        return d_out_all
