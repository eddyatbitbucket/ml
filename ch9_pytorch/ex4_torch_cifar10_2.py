import torch
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np

#%% 載入資料

transform = transforms.Compose([transforms.ToTensor(),
                                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

batch_size=100
trainset = torchvision.datasets.CIFAR10(root='./data',
                                        train=True,
                                        download=True,
                                        transform=transform)
trainloader = torch.utils.data.DataLoader(trainset,
                                          batch_size=batch_size,
                                          shuffle=True,
                                          num_workers=2)
train_size = len(trainloader)

testset = torchvision.datasets.CIFAR10(root='./data',
                                       train=False,
                                       download=True,
                                       transform=transform)
testloader = torch.utils.data.DataLoader(testset,
                                         batch_size=batch_size,
                                         shuffle=False,
                                         num_workers=2)

classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')


def imshow(img):
    img = img / 2 + 0.5
    plt.imshow(np.transpose(img.numpy(), (1, 2, 0)))
    plt.show()


dataiter = iter(trainloader)
imgs, labels = dataiter.next()
img_grid = torchvision.utils.make_grid(imgs)
imshow(img_grid)

print(' '.join([classes[labels[i]] for i in range(imgs.shape[0])]))

#%% Cuda

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
cudnn_version = torch.backends.cudnn.version()
f'device:{device}, cudnn version: {cudnn_version}'

# %% 定義神經網路
import torch.nn as nn
import torch.nn.functional as F


class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=128, kernel_size=3)
        self.conv2 = nn.Conv2d(in_channels=128, out_channels=64, kernel_size=3)
        self.conv3 = nn.Conv2d(in_channels=64, out_channels=32, kernel_size=3)
        self.conv4 = nn.Conv2d(in_channels=32, out_channels=16, kernel_size=3)
        self.conv5 = nn.Conv2d(in_channels=16, out_channels=8, kernel_size=3)
        self.fc1 = nn.Linear(8 * 22 * 22, 128)
        self.fc2 = nn.Linear(128, 64)
        self.fc3 = nn.Linear(64, 10)

    def forward(self, x):
        x = self.conv1(x)
        x = F.relu(x)
        x = self.conv2(x)
        x = F.relu(x)
        # x = F.max_pool2d(x, kernel_size=2)
        x = self.conv3(x)
        x = F.relu(x)
        x = self.conv4(x)
        x = F.relu(x)
        x = self.conv5(x)
        x = F.relu(x)

        x = x.view(-1, 8 * 22 * 22)
        x = self.fc1(x)
        x = F.relu(x)
        x = self.fc2(x)
        x = F.relu(x)
        x = self.fc3(x)

        return x


net = Net().to(device)

import torch.optim as optim

loss_function = nn.CrossEntropyLoss()
optimizer = optim.Adam(net.parameters())

# %% 訓練網路

epoch_num = 68
plot_freq = 100

for epoch_index in range(epoch_num):

    loss_sum = 0.0

    for n, data in enumerate(trainloader):
        x, t = data
        x = x.to(device)
        t = t.to(device)

        optimizer.zero_grad()

        y = net(x)
        loss = loss_function(y, t)
        loss.backward()
        optimizer.step()
        loss_sum += loss.item()
        loss_mean = loss_sum / plot_freq
        if n % plot_freq == plot_freq - 1:
            print(f'epoch: {epoch_index+1}, {(n+1)*batch_size}/{train_size*batch_size}, loss: {loss_mean}')
            loss_sum = 0.0

print('finished training')

# 預測
test_iter = iter(testloader)
imgs, labels = test_iter.next()
imshow(torchvision.utils.make_grid(imgs))
print('t: ', ' '.join(classes[labels[j]] for j in range(imgs.shape[0])))

y = net(imgs.to(device))
_, predict = torch.max(y, 1)
print('y: ', ' '.join(classes[predict[k]] for k in range(imgs.shape[0])))

correct = 0
total = 0
with torch.no_grad():
    for data in testloader:
        imgs, labels = data
        imgs, labels = imgs.to(device), labels.to(device)
        y = net(imgs)
        _, predict = torch.max(y.data, 1)
        total += labels.size(0)
        correct += (predict == labels).sum().item()

print(f'accuracy: {correct/total}')
