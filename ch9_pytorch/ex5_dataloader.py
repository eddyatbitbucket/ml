from __future__ import print_function, division
import os
import torch
import pandas as pd
from skimage import io, transform
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils

import warnings

warnings.filterwarnings('ignore')

# plt.ion()

# %% 讀取檔案，轉為dataframe
df = pd.read_csv("./data/faces/face_landmarks.csv")
row1 = df.iloc[0]
img_name = row1.loc['image_name']
landmarks = row1.iloc[1:].values.reshape(-1, 2)


# %% 展示圖片
def show_landmarks(img, landmarks):
    plt.imshow(img)
    plt.scatter(landmarks[:, 0], landmarks[:, 1], s=10, marker='.', c='r')


plt.figure()
img = io.imread(os.path.join('./data/faces/', img_name))
show_landmarks(img, landmarks)
plt.show()


# %% 自製dataset
class MyDataset(Dataset):
    def __init__(self, csv_file, root_dir, transform=None):
        csv_url = os.path.join(root_dir, csv_file)
        self.df = pd.read_csv(csv_url)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        img_name = os.path.join(self.root_dir,
                                self.df.iloc[idx, 0])
        img = io.imread(img_name)
        landmarks = self.df.iloc[idx, 1:].values
        landmarks = landmarks.astype('float').reshape(-1, 2)
        sample = [img, landmarks]

        if self.transform:
            sample = self.transform(sample)

        return sample


# %% 操作dataset

dataset = MyDataset(csv_file='face_landmarks.csv',
                    root_dir='./data/faces/')
fig = plt.figure()
for i in range(len(dataset)):
    sample = dataset[i]
    # print(i, sample['img'].shape, sample['landmarks'].shape)
    ax = plt.subplot(1, 4, i + 1)
    plt.tight_layout()
    ax.set_title(f'sample {i}')
    ax.axis('off')
    show_landmarks(sample[0], sample[1])

    if i == 3:
        plt.show()
        break


# %% Transforms
class Rescale():
    def __init__(self, w, h):
        self.w = int(w)
        self.h = int(h)

    def __call__(self, sample):
        img, landmarks = sample
        # img形狀為 H,W,C
        h0, w0 = img.shape[:2]
        img = transform.resize(img, (self.h, self.w))
        landmarks = landmarks * [self.w / w0, self.h / h0]
        return img, landmarks


# %% 原大小
sample = dataset[0]
img, landmarks = sample
plt.imshow(img)
plt.show()

# %%縮放
sample = dataset[0]
rescale = Rescale(128, 128)
img, landmarks = rescale(sample)
plt.imshow(img)
plt.scatter(landmarks[:, 0], landmarks[:, 1], c='red', marker=".")
plt.show()


# %% random crop
class RandomCrop():
    def __init__(self, crop_w, crop_h):
        self.w = int(crop_w)
        self.h = int(crop_h)

    def __call__(self, sample):
        img, landmarks = sample
        h0, w0 = img.shape[:2]
        top = np.random.randint(0, h0 - self.h)
        left = np.random.randint(0, w0 - self.w)
        img = img[top:top + self.h, left:left + self.w]
        landmarks -= [left, top]
        return img, landmarks


sample = dataset[0]
random_crop = RandomCrop(100, 100)
img, landmarks = random_crop(sample)
plt.imshow(img)
plt.scatter(landmarks[:, 0], landmarks[:, 1])
plt.show()

# %% compose
sample = dataset[0]
composed_transforms = transforms.Compose([rescale, random_crop])
sample = composed_transforms(sample)
plt.imshow(sample[0])
plt.show()

# %% Torchvision ImageFolder
import torch
import torchvision

torchvision_dataset = torchvision.datasets.ImageFolder(
    root='./data/torchvision_dataset')
dataloader = torch.utils.data.DataLoader(torchvision_dataset, batch_size=4,
                                         shuffle=True,
                                         num_workers=4)
