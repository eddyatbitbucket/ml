import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
import time
import os
import copy
import matplotlib.pyplot as plt

# download: https://download.pytorch.org/tutorial/hymenoptera_data.zip

# %% data preprocessing
mean, sd = [0.485, 0.456, 0.406], [0.229, 0.224, 0.225]
phases = ['train', 'val']
data_transforms = {
    phases[0]: transforms.Compose([
        transforms.RandomResizedCrop(224),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(mean, sd)
    ]),
    phases[1]: transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean, sd)
    ])
}

data_dir = './data/hymenoptera_data'

image_datasets = {x: datasets.ImageFolder(root=os.path.join(data_dir, x),
                                          transform=data_transforms[x])
                  for x in phases}
dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x],
                                              batch_size=50,
                                              shuffle=True,
                                              num_workers=4)
               for x in phases}
dataset_sizes = {x: len(image_datasets[x]) for x in phases}
class_names = image_datasets['train'].classes
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


# %% visulize
def imshow(im, title=None):
    im = im.numpy().transpose((1, 2, 0))
    m = np.array(mean)
    s = np.array(sd)
    im = s * im + mean
    im = np.clip(im, 0, 1)
    plt.imshow(im)
    if title is not None:
        plt.title(title)
    plt.show()


XX, TT = next(iter(dataloaders['train']))
im_grid = torchvision.utils.make_grid(XX)
imshow(im_grid, title=[class_names[t] for t in TT])


# %% training
def train_model(model, loss_fn, optimizer, scheduler, num_epochs=25):
    best_model_wts = copy.deepcopy(model.state_dict())
    best_accuracy = 0.0

    for epoch in range(num_epochs):
        print(f'Epoch {epoch}/{num_epochs-1}')

        for phase in phases:
            if phase == 'train':
                scheduler.step()
                model.train()  # set train mode
            else:
                model.eval()  # set eval mode

            running_loss = 0.0
            running_correct = 0

            for x, t in dataloaders[phase]:
                x = x.to(device)
                t = t.to(device)

                optimizer.zero_grad()

                with torch.set_grad_enabled(phase == 'train'):
                    y_one_hot = model(x)

                    # 返回dim =1中，每一列中最大值的那个元素，且返回索引
                    _, y_index = torch.max(y_one_hot, 1)
                    loss = loss_fn(y_one_hot, t)

                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                running_loss += loss.item() * x.size(0)
                running_correct += torch.sum(y_index == t.data)

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_accuracy = running_correct.double() / dataset_sizes[phase]

            print(f'{phase} loss: {epoch_loss:.4}, accuracy: {epoch_accuracy:.4}')

            if phase == 'val' and epoch_accuracy > best_accuracy:
                best_accuracy = epoch_accuracy
                best_model_wts = copy.deepcopy(model.state_dict())

    print(f'best val accuracy: {best_accuracy:.4}')
    model.load_state_dict(best_model_wts)

    return model


# %% finetune the transferred model
resnet = models.resnet18(pretrained=True)
fc_input_num = resnet.fc.in_features  # fc是resnet的最後一層
resnet.fc = nn.Linear(fc_input_num, 2)  # 改成輸出為2

resnet = resnet.to(device)
loss_fn = nn.CrossEntropyLoss()

optimizer = optim.SGD(resnet.parameters(), lr=0.001, momentum=0.9)

decaying_lr_scheduler = lr_scheduler.StepLR(optimizer, step_size=7, gamma=0.1)

# %% train
resnet = train_model(resnet,
                     loss_fn=loss_fn,
                     optimizer=optimizer,
                     scheduler=decaying_lr_scheduler,
                     num_epochs=10)


# %% visualize

def visualize_model(model, num_images=6):
    was_training = model.training
    model.eval()
    images_so_far = 0

    with torch.no_grad():
        for i, (inputs, labels) in enumerate(dataloaders['val']):
            inputs = inputs.to(device)
            labels = labels.to(device)

            outputs = model(inputs)
            _, preds = torch.max(outputs, 1)

            for j in range(inputs.size()[0]):
                images_so_far += 1
                plt.title('predicted: {}'.format(class_names[preds[j]]))
                imshow(inputs.cpu().data[j])

                if images_so_far == num_images:
                    model.train(mode=was_training)
                    return

        model.train(mode=was_training)


visualize_model(resnet)
