import torch
import torchvision
import torchvision.transforms as transforms
import numpy as np
import os
import copy
import matplotlib

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

data_dir = './data/hymenoptera_data'

phases = ['train', 'val']
mean, sd = [0.485, 0.456, 0.406], [0.229, 0.224, 0.225]

data_transforms = {
    phases[0]: transforms.Compose([
        transforms.Resize(256),
        transforms.RandomResizedCrop(224),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(mean, sd)
    ]),
    phases[1]: transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean, sd)
    ])
}
img_datasets = {x: torchvision.datasets.ImageFolder(root=os.path.join(data_dir, x),
                                                    transform=data_transforms[x])
                for x in phases}

data_loaders = {x: torch.utils.data.DataLoader(img_datasets[x],
                                               batch_size=int(50),
                                               shuffle=True,
                                               num_workers=8)
                for i, x in enumerate(phases)}

dataset_sizes = {x: len(img_datasets[x]) for x in phases}
class_names = img_datasets['train'].classes  # ['ant', 'bee']

# %% 繼承ResNet
from torch.utils import model_zoo
from torchvision.models import resnet
import torch.nn.functional as F


class MyResNet(torchvision.models.ResNet):
    def __init__(self, block, layers):
        super().__init__(block, layers)
        self.feature_maps = None
        self.best_accuracy = 0.0

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        # 複寫forward()，新增保存avgpool前的feature_map
        self.feature_maps = x

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        x = F.softmax(x, dim=1)

        return x, self.feature_maps

    def set_fc_classification_num(self, classification_num):
        self.fc = torch.nn.Linear(self.fc.in_features, classification_num)

    def load_pretrained_data(self):
        self.load_state_dict(model_zoo.load_url(resnet.model_urls['resnet18']))


my_resnet = MyResNet(torchvision.models.resnet.BasicBlock, [2, 2, 2, 2])
my_resnet.load_pretrained_data()
my_resnet.set_fc_classification_num(2)  # 把最後一層fc的分類數目改成2個

my_resnet.to(device)

# %%
loss_fn = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(my_resnet.parameters(), lr=1e-5)

# %% train
import utility


def train(model, loss_fn, optimizer, num_epoch=20):
    best_model_state = copy.deepcopy(model.state_dict())

    t0 = utility.Time.get_epoch_time_second()

    for epoch in range(num_epoch):
        print(f'{epoch}/{num_epoch}')

        for phase in phases:
            if phase == 'train':
                optimizer.step()
                model.train(mode=True)
            else:
                model.train(mode=False)

            loss_sum = 0
            accuracy_sum = 0

            for x, t in data_loaders[phase]:
                x = x.to(device)
                t = t.to(device)
                optimizer.zero_grad()

                with torch.set_grad_enabled(phase == 'train'):
                    y, feature_maps = model(x)
                    _, y_cls = torch.max(y, 1)
                    loss = loss_fn(y, t)
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()
                loss_sum += loss.item() * x.size(0)
                accuracy_sum += torch.sum(y_cls == t.data)

            n = dataset_sizes[phase]
            phase_loss = loss_sum / n
            phase_accuracy = accuracy_sum.double() / n

            print(f'{phase:>5} loss: {phase_loss:=.4}, accuracy: {phase_accuracy:=.4}')

            if phase == 'val' and phase_accuracy > model.best_accuracy:
                model.best_accuracy = phase_accuracy
                best_model_state = copy.deepcopy(model.state_dict())
                torch.save({'accuracy': model.best_accuracy,
                            'model_state': best_model_state}
                           , model_save_path)
                print('best model saved!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')

        print(f'           best val accuracy: {model.best_accuracy:=.4}')
        t1 = utility.Time.get_epoch_time_second()
        t10 = t1 - t0
        rate = (epoch + 1) / t10
        residual_time = (num_epoch + 1 - epoch) / rate
        print(f'預估時間: {int(residual_time//60//60)}hr '
              f'{int(residual_time//60%60)}min '
              f'{int(residual_time%60)}sec')
    model.load_state_dict(best_model_state)

    return model


# %% Load model
model_save_path = './my_resnet_state_dict.pkl'
if os.path.exists(model_save_path):
    model_dict = torch.load(model_save_path)
    previous_accuracy = model_dict['accuracy']
    model_state = model_dict['model_state']
    if previous_accuracy > my_resnet.best_accuracy:
        my_resnet.load_state_dict(model_state)

        print(f'model loaded, '
              f'previous best accuracy={previous_accuracy:.4}'
              f'>{my_resnet.best_accuracy:.4}')

        my_resnet.best_accuracy = previous_accuracy
else:
    # %% Train *****************************************************************
    my_resnet = train(my_resnet, loss_fn, optimizer, num_epoch=100)

# %% visulize =================================================================
import matplotlib.pyplot as plt
from visualization import get_heatmap


def imshow(img, heatmap, probability, title=None):
    img = img.numpy().transpose((1, 2, 0))
    m = np.array(mean)
    s = np.array(sd)
    img = s * img + mean
    img = np.clip(img, 0, 1)
    bg_img_rgba = np.append(img, np.ones((img.shape[0], img.shape[1], 1)), axis=2)

    fg_img_rbga = np.ones(bg_img_rgba.shape)
    rgba_opacity = [1, 1, 0, 0.6]
    for c in range(3):
        fg_img_rbga[:, :, c] = heatmap

    blended_img = get_heatmap(fg_img_rbga, bg_img_rgba, rgba_channel=rgba_opacity)

    plt.imshow(blended_img)
    plt.title(f'{title}, probability: {probability*100:.2f}%')
    plt.show()


import scipy.ndimage


def pytorch_visualize_model(model, dataloaders, class_names, num_images=6):
    is_cuda = next(model.parameters()).is_cuda
    device = torch.device('cuda' if is_cuda else 'cpu')

    was_training = model.training
    model.eval()
    images_so_far = 0

    with torch.no_grad():
        for i, (inputs, labels) in enumerate(dataloaders['val']):
            inputs = inputs.to(device)
            imgs = inputs.cpu()

            outputs, feature_maps = model(inputs)

            # heatmap
            feature_maps = feature_maps.cpu().numpy()
            fm = feature_maps.transpose(0, 2, 3, 1)
            fc_weights = model.fc.weight.cpu().numpy()
            fc_bias = model.fc.bias.cpu().numpy()

            probabilites, preds = torch.max(outputs, 1)

            for j in range(inputs.size()[0]):
                images_so_far += 1

                heat_maps = fm * fc_weights[preds[j]] + fc_bias[preds[j]]
                heat_maps = np.sum(heat_maps, axis=3)

                hm = heat_maps[j]
                hm = (hm - np.min(hm)) / np.ptp(hm)
                hm = scipy.ndimage.zoom(hm, 32, order=2)

                title = class_names[preds[j]]
                imshow(imgs.data[j], hm, probabilites[preds[j]], title)

                if images_so_far == num_images:
                    model.train(mode=was_training)
                    return

        model.train(mode=was_training)


pytorch_visualize_model(my_resnet, data_loaders, class_names, num_images=6)
