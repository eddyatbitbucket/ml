#!/usr/bin/env python
# coding: utf-8

# In[3]:


import torch as t
from torch import nn
from torch.autograd import Variable


class Linear(nn.Module):
    def __init__(self, in_features, out_features):
        super(Linear, self).__init__()
        self.w = nn.Parameter(t.randn(in_features, out_features))
        self.b = nn.Parameter(t.randn(out_features))
        
    def forward(self, x):
        x = x.mm(self.w)
        return x + self.b.expand_as(x)


# In[6]:


layer= Linear(4,3)
input=Variable(t.randn(2,4))
output=layer(input)
output


# In[7]:


for name, parameter in layer.named_parameters():
    print(name, parameter)


# In[ ]:




