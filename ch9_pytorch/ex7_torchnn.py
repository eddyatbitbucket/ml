import torch

device = torch.device('cuda')
N, Din, H, Dout = 64, 10000, 10000, 10

X = torch.randn(N, Din, device=device)
T = torch.randn(N, Dout, device=device)

# %% sequential
nn = torch.nn.Sequential(
    torch.nn.Linear(Din, H),
    torch.nn.ReLU(),
    torch.nn.Linear(H, Dout),
)

nn.to(device)

loss_fn = torch.nn.MSELoss(reduction='sum')

# %% SGD
lr = 1e-4
for i in range(500):
    Y = nn(X)
    loss = loss_fn(Y, T)
    print(i, loss.item())
    loss.backward()

    with torch.no_grad():
        for param in nn.parameters():
            param -= lr * param.grad

        nn.zero_grad()

# %% Optim Adam
optimizer = torch.optim.Adam(nn.parameters(), lr=lr)
for i in range(500):
    Y = nn(X)
    loss = loss_fn(Y, T)
    print(i, loss.item())

    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

# %% 自訂 nn.Module
import torch

device = torch.device('cuda')


class MyNet(torch.nn.Module):
    def __init__(self, Din, H, Dout):
        super(MyNet, self).__init__()
        self.linear1 = torch.nn.Linear(Din, H)
        self.linear2 = torch.nn.Linear(H, Dout)

    def forward(self, x):
        y1 = self.linear1(x)
        y1_relu = y1.clamp(min=0)
        y = self.linear2(y1_relu)
        return y


N, Din, H, Dout = 64, 1000, 100, 10
X = torch.randn(N, Din, device=device)
T = torch.randn(N, Dout, device=device)
nn = MyNet(Din, H, Dout)

nn.to(device)

loss_fn = torch.nn.MSELoss()
optimizer = torch.optim.Adam(nn.parameters(), lr=1e-4)
for i in range(500):
    Y = nn(X)
    loss = loss_fn(Y, T)
    print(i, loss)

    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

# %% 動態改變網路結構

import torch
import torch.nn.functional as F
import random
import torchsummary


class DynamicNet(torch.nn.Module):
    def __init__(self, in_size, h_size, out_size):
        super().__init__()
        self.linear1 = torch.nn.Linear(in_size, h_size)
        self.linear2 = torch.nn.Linear(h_size, h_size)
        self.linear3 = torch.nn.Linear(h_size, out_size)

    def forward(self, x):
        x1 = self.linear1(x)
        x2 = F.relu(x1)

        random_layer_num = 0
        for i in range(random.randint(1, 3)):
            x3 = self.linear2(x2)
            x4 = F.relu(x3)
            random_layer_num = i + 1
        y = self.linear3(x4)
        print(f'random layer num: {random_layer_num}')

        return y


device = 'cuda' if torch.cuda.is_available() else 'cpu'
N, Din, H, Dout = 64, 1000, 100, 10

X = torch.randn(N, Din, device=device)
T = torch.randn(N, Dout, device=device)

dynamic_net = DynamicNet(Din, H, Dout)
dynamic_net.to(device)

loss_fn = torch.nn.MSELoss(reduction='sum')
optimizer = torch.optim.SGD(dynamic_net.parameters(), lr=1e-4, momentum=0.9)

for i in range(500):
    Y = dynamic_net.forward(X)

    loss = loss_fn(Y, T)
    print(i, loss)
    # torchsummary.summary(dynamic_net, input_size=(64, Din), batch_size=N)
    # print(dynamic_net)
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()
