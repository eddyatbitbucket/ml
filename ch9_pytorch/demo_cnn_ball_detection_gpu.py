# -*- coding: utf-8 -*-
import os
import sys

import torch

curPath = os.path.abspath(os.path.dirname(__file__))
project_root = os.path.dirname(curPath)
sys.path.insert(0, project_root)
print(sys.path)
# 在sys.path還未設定之前，不能執行其他import，否則可能會有module not found exception


import numpy as np

from dataset import lnd_dataset

# %% Cuda

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
cudnn_version = torch.backends.cudnn.version()
f'device:{device}, cudnn version: {cudnn_version}'

# %% Load data
# (x_train, t_train), (x_test, t_test) = random_ball.get_sample()
(x_train, t_train_one_hot), (x_test, t_test_one_hot) = lnd_dataset.load(use_cache=True, plot_while_doing=False)

# %% data loader
import torch.utils.data

x_train = torch.Tensor(x_train)
t_train = np.argmax(t_train_one_hot, 1)
t_train = torch.LongTensor(t_train)
train_dataset = torch.utils.data.TensorDataset(x_train, t_train)
batch_size = 10
train_size = t_train.shape[0]
train_loader = torch.utils.data.DataLoader(
    dataset=train_dataset,
    batch_size=batch_size,
    shuffle=True,
    num_workers=1
)

x_test = torch.Tensor(x_test)
t_test = np.argmax(t_test_one_hot, 1)
t_test = torch.LongTensor(t_test)
test_dataset = torch.utils.data.TensorDataset(x_test, t_test)
test_loader = torch.utils.data.DataLoader(
    dataset=test_dataset,
    batch_size=batch_size,
    shuffle=False,
    num_workers=1
)

# %% 定義神經網路
import torch.nn as nn
import torch.nn.functional as F


class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv3d(in_channels=1, out_channels=10, kernel_size=3, padding=1)
        self.conv2 = nn.Conv3d(in_channels=10, out_channels=10, kernel_size=3, padding=1)
        self.conv3 = nn.Conv3d(in_channels=10, out_channels=10, kernel_size=3, padding=1)
        # self.conv2 = nn.Conv2d(in_channels=128, out_channels=64, kernel_size=3)

        self.fc1 = nn.Linear(10 * 32 * 32 * 7, 2)

    def forward(self, x):
        x = self.conv1(x)
        x = F.relu(x)
        x = self.conv2(x)
        x = F.relu(x)

        x = self.conv3(x)
        x = F.relu(x)
        x = x.view(-1, 10 * 32 * 32 * 7)
        x = self.fc1(x)

        return x


my_model = Net().to(device)

# %% optimizer
import torch.optim as optim

loss_function = nn.CrossEntropyLoss()
optimizer = optim.Adam(my_model.parameters(), lr=0.0001)

# %% 訓練網路

epoch_num = 100
plot_freq = 100

for epoch_index in range(epoch_num):

    loss_sum = 0.0

    for n, data in enumerate(train_loader):
        x, t = data
        x = x.to(device)
        t = t.to(device)

        optimizer.zero_grad()

        y = my_model(x)
        loss = loss_function(y, t)
        loss.backward()
        optimizer.step()
        loss_sum += loss.item()
        loss_mean = loss_sum / plot_freq
        if n % plot_freq == plot_freq - 1:
            print(f'epoch: {epoch_index+1}, {(n+1)*batch_size}/{train_size}, loss: {loss_mean}')
            loss_sum = 0.0

print('finished training')

# 預測
test_iter = iter(test_loader)
imgs, labels = test_iter.next()

print('t: ', labels)

y = my_model(imgs.to(device))
_, predict = torch.max(y, 1)
print('y: ', predict)

correct = 0
total = 0
with torch.no_grad():
    for data in test_loader:
        imgs, labels = data
        imgs, labels = imgs.to(device), labels.to(device)
        y = my_model(imgs)
        _, predict = torch.max(y.data, 1)
        total += labels.size(0)
        correct += (predict == labels).sum().item()

print(f'accuracy: {correct/total}')
