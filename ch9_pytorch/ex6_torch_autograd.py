import torch

# device = torch.device("cuda")
device = torch.device("cpu")

N, Din, H, Dout = 1000, 64, 32, 10
x = torch.randn(N, Din, device=device, dtype=torch.float)
t = torch.randn(N, Dout, device=device, dtype=torch.float)

w1 = torch.randn(Din, H, device=device, dtype=torch.float, requires_grad=True)
w2 = torch.randn(H, Dout, device=device, dtype=torch.float, requires_grad=True)

lr=0.0001
for i in range(10000):
    y1 = x.mm(w1)
    y2 = y1.clamp(min=0)
    y3 = y2.mm(w2)

    loss = (t - y3).pow(2).sum()/N
    print(i, loss)
    loss.backward()

    with torch.no_grad():
        w1-=lr*w1.grad
        w2-=lr*w2.grad

        w1.grad.zero_()
        w2.grad.zero_()