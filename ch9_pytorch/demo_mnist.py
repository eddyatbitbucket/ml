from __future__ import print_function

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 128, kernel_size=3, padding=1)
        self.conv2 = nn.Conv2d(128, 64, kernel_size=3, padding=1)
        self.conv3 = nn.Conv2d(64, 32, kernel_size=3, padding=1)
        self.conv4 = nn.Conv2d(32, 16, kernel_size=3, padding=1)
        self.conv5 = nn.Conv2d(16, 16, kernel_size=3, padding=1)
        # self.conv6 = nn.Conv2d(16, 16, kernel_size=3, padding=1)
        # self.conv7 = nn.Conv2d(16, 16, kernel_size=3, padding=1)
        # self.conv8 = nn.Conv2d(16, 16, kernel_size=3, padding=1)
        # self.conv9 = nn.Conv2d(16, 16, kernel_size=3, padding=1)
        # self.conv10 = nn.Conv2d(16, 16, kernel_size=3, padding=1)
        # self.conv11 = nn.Conv2d(16, 16, kernel_size=3, padding=1)
        # self.conv12 = nn.Conv2d(16, 16, kernel_size=3, padding=1)
        # self.conv13 = nn.Conv2d(16, 16, kernel_size=3, padding=1)
        # self.conv14 = nn.Conv2d(16, 16, kernel_size=3, padding=1)
        # self.conv15 = nn.Conv2d(16, 16, kernel_size=3, padding=1)
        self.dropout = nn.Dropout2d()
        self.fc1 = nn.Linear(16 * 14 * 14, 40)
        self.fc2 = nn.Linear(40, 10)

    def forward(self, x):
        x = self.conv1(x)
        x = F.relu(x)

        x = self.conv2(x)
        x = F.relu(x)

        x = self.conv3(x)
        x = F.relu(x)

        x = self.conv4(x)
        x = F.relu(x)

        x = self.conv5(x)
        x = F.relu(x)

        # x = self.conv6(x)
        # x = F.relu(x)
        #
        # x = self.conv7(x)
        # x = F.relu(x)
        #
        # x = self.conv8(x)
        # x = F.relu(x)
        #
        # x = self.conv9(x)
        # x = F.relu(x)
        #
        # x = self.conv10(x)
        # x = F.relu(x)
        #
        # x = self.conv11(x)
        # x = F.relu(x)
        #
        # x = self.conv12(x)
        # x = F.relu(x)
        #
        # x = self.conv13(x)
        # x = F.relu(x)
        #
        # x = self.conv14(x)
        # x = F.relu(x)
        #
        # x = self.conv15(x)
        # x = F.relu(x)

        x = self.dropout(x)
        x = F.max_pool2d(x, kernel_size=2)

        x = x.view(-1, 16 * 14 * 14)

        x = self.fc1(x)
        x = F.relu(x)

        x = self.fc2(x)
        return F.log_softmax(x, dim=1)


def train(model, device, train_loader, optimizer, epoch):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % 100 == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                       100. * batch_idx / len(train_loader), loss.item()))


def test(model, device, test_loader):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            test_loss += F.nll_loss(output, target, reduction='sum').item()  # sum up batch loss
            pred = output.max(1, keepdim=True)[1]  # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_loader.dataset)
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))


def main():
    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")
    print('device:', device)

    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    train_loader = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=True, download=True,
                       transform=transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,), (0.3081,))
                       ])),
        batch_size=64, shuffle=True, **kwargs)
    test_loader = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=False, transform=transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
        ])),
        batch_size=1000, shuffle=True, **kwargs)

    model = Net().to(device)

    optimizer = optim.SGD(model.parameters(), lr=0.01, momentum=0.5)
    # optimizer=optim.Adam(model.parameters())
    for epoch in range(100):
        train(model, device, train_loader, optimizer, epoch)
        test(model, device, test_loader)


main()
