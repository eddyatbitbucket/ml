import numpy as np


def numerical_diff(f, x):
    h = 0.0001

    return (f(x + h) - f(x - h)) / (2 * h)


def numerical_gradient(f, x):
    h = 1e-4  # 0.0001
    G = np.zeros_like(x)

    it = np.nditer(x, flags=['multi_index'], op_flags=['readwrite'])
    while not it.finished:
        i = it.multi_index
        x0 = x[i]
        x[i] = x0 + h
        fxh1 = f(x[i])

        x[i] = x0 - h
        fxh0 = f(x[i])
        G[i] = (fxh1 - fxh0) / (2 * h)

        x[i] = x0
        it.iternext()

    return G


def gradient_descent_with_history(f, init_x, learning_rate=0.01, step_num=100):
    x = init_x
    x_history = []

    for i in range(step_num):
        x_history.append(x.copy())

        grad = numerical_gradient(f, x)
        x -= learning_rate * grad

    return x, np.array(x_history)
