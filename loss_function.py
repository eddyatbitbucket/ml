import numpy as np


def cross_entropy(Y, T):
    """交叉熵
    :param: Y: nparray(float) 神經網路的輸出, ex: [0.5, 0.7, 0.1,...]
    :param: T: nparray(int) 訓練集的ground truth, ex: [1,0,0,...]
    :return: -sum(T * ln(Y))/m

    代表神經網路輸出與訓練集正確答案的差距，越小代表預測越精確
    CEL(y,t) = -y ln(t) - (1-y) ln(1-t)
    J(w) = -1/N sigma(CEL)

    最準確的狀況是t=1, y=1 --> 1 * ln(1) = 0
    其他有誤差的狀況都是大於零的
    """
    m = Y.shape[0]
    result = -np.sum(np.log(Y) * T) / m

    return result


def mean_square_error(Y, T):
    """均方差
    代表神經網路輸出與訓練集正確答案的差距，越小代表預測越精確

    y: 神經網路的輸出, ex: 0.5
    t: 訓練集的答案, ex: 1
    return: mean((Y-T)**2)

    最準確的狀況是t=y --> (t-y)^2 = 0
    其他有誤差的狀況都會大於0
    """

    return np.mean((Y - T) ** 2)
