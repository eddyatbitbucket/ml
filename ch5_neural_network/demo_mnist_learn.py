# -*- coding: utf-8 -*-
import os
import sys

curPath = os.path.abspath(os.path.dirname(__file__))
project_root = os.path.dirname(curPath)
sys.path.insert(0, project_root)
# 在sys.path還未設定之前，不能執行其他import，否則可能會有module not found exception


from dataset.mnist import load_mnist
from neural_network import NeuralNetwork
from neural_network import Affine
from neural_network import Relu
from neural_network import SoftmaxWithLoss

(x_train, t_train), (x_test, t_test) = \
    load_mnist(normalize=True, one_hot_label=True)

# layer setting
affine1 = Affine(256)
relu1 = Relu()
affine2 = Affine(32)
relu2 = Relu()
affine = Affine(10)
last_layer = SoftmaxWithLoss()

layers = [affine1, relu1,
          affine2, relu2,
          affine, last_layer]

nn = NeuralNetwork(layers)

nn.learn(x_train, t_train)

accuracy = nn.accuracy(x_test, t_test)
print(accuracy)

# a2_w_imgs = (affine2.W[1].reshape(4, 4)+0.5)*255
# show_image(a2_w_imgs)
# img=x_train[0].reshape(28,28)*255
# show_image(img)
