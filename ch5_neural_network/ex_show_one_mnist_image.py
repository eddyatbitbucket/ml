import sys, os

sys.path.append(os.pardir)
import dataset.mnist
from visualization import show_image


(x_train, t_train), (x_test, t_test) = \
    dataset.mnist.load_mnist(flatten=True, normalize=False)

label = t_train[0]
print(label)
img = x_train[0]
img = img.reshape(28, 28)
show_image(img)
