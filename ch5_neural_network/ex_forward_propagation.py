import numpy as np
import neural_network as NN

affine1 = NN.Affine(2, 3)
affine1.W = np.array([[0.1, 0.3, 0.5], [0.2, 0.4, 0.6]])
affine1.B = np.array([0.1, 0.2, 0.3])

relu1 = NN.Relu()

affine2 = NN.Affine(3, 2)
affine2.W = np.array([[0.1, 0.4], [0.2, 0.5], [0.3, 0.6]])
affine2.B = np.array([0.1, 0.2])

last_layer = NN.SoftmaxWithLoss()

layers = [affine1, relu1, affine2, last_layer]

network = NN.NeuralNetwork(layers)

T = np.array([[0, 1], [1, 0]])

Loss, Y = network.forward_all(np.array([[1.0, 0.5], [1.0, 0.5]]), T)

print("Y: ", Y)

print('loss: ', Loss)
