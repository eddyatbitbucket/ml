import sys, os

sys.path.append(os.pardir)
import dataset.mnist
from visualization import show_multiple_images


(x_train, t_train), (x_test, t_test) = \
    dataset.mnist.load_mnist(flatten=True, normalize=False)

imgs = x_train[:64]
reshaped_images = list()
for i in imgs:
    reshaped_images.append(i.reshape(28, 28))

show_multiple_images(reshaped_images, 10)
