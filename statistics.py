import matplotlib.pyplot as plt


# import matplotlib.animation as animation


def plot_loss(loss_history):
    plt.plot(loss_history)
    plt.show()


def plot_accuracy(accuracy_history):
    plt.plot(accuracy_history)
    plt.show()


def plot_all(loss_history, accuracy_history):
    plt.figaspect(1.6)
    plt.subplot(2, 1, 1)
    plt.plot(loss_history)
    plt.ylabel(f'loss={round(loss_history[-1], 4)}')

    plt.subplot(2, 1, 2)
    plt.plot(accuracy_history)
    plt.ylabel(f'accuracy={round(accuracy_history[-1]*100,2)}%')
    plt.show()


fig = plt.figure()
ax1 = fig.add_subplot(2, 1, 1)

#
# def plot_animated(loss_history, accuracy_history):
#     plt.figaspect(1.6)
#     plt.subplot(2, 1, 1)
#     plt.plot(loss_history)
#     plt.ylabel('loss')
#
#     plt.subplot(2, 1, 2)
#     plt.plot(accuracy_history)
#     plt.ylabel('accu')
#     plt.show()
#
#
# ani = animation.FuncAnimation(fig, plot_animated, interval=1000)
