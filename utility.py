import csv
import datetime
import inspect
import os
import time


log_enabled = True


def read_csv(file_name):
    import csv
    # , encoding='utf-8'
    with open(file_name, 'r', encoding='utf-8') as file:
        reader = csv.reader(file)
        data_rows = [row for row in reader]
    file.close()
    return data_rows


def write_csv(data_rows, file_url):
    directory = os.path.dirname(file_url)
    if not os.path.exists(directory):
        os.makedirs(directory)

    mode = 'a' if os.path.exists(file_url) else 'w'

    with open(file_url, mode, newline='') as f:
        csv_cursor = csv.writer(f)
        csv_cursor.writerow(data_rows)
        return True

    return False


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def log(*messages, clear=False):
    if log_enabled:
        if clear:
            os.system('clear')
        # 取得呼叫這個函式的檔案名稱
        frame = inspect.stack()[1]
        file = inspect.getmodule(frame[0])

        print("{}log: {} ({}):{}".format(bcolors.OKBLUE, file.__name__, Time.get_date(), Time.get_hms(), bcolors.ENDC, end=' '))

        is_first_message = True
        for m in messages:
            if is_first_message:
                print(m, end=' ')
                is_first_message = False
            else:
                # print(" ", end='')
                print(m, end=' ')
        print("")
        print(bcolors.OKGREEN + '========================================================================' + bcolors.ENDC)


def log_progress(ratio, *messages):
    percentage = "{}%".format(round(ratio * 100))
    progress_bar_length = 50
    progress_number = round(ratio * progress_bar_length)
    progress_bar = "{}".format("█" * progress_number + "▒" * (progress_bar_length - progress_number))
    log(progress_bar, percentage, messages)


def split_word_keep_delimiter(delimiter, text):
    """把例如 and/or 拆成 ['and', '/', 'or'] """
    if delimiter == " ":
        return text.split()
    if delimiter == "":
        return [text]

    if delimiter in text:
        words_without_delimiter = text.split(delimiter)
        words_with_delimiter = list()
        for i in range(len(words_without_delimiter)):
            words_with_delimiter.append(words_without_delimiter[i])
            if i < (len(words_without_delimiter) - 1):
                words_with_delimiter.append(delimiter)
        return words_with_delimiter
    else:
        return [text]


class Time:
    @staticmethod
    def get_hms():
        return datetime.datetime.now().strftime('%H:%M:%S.%f')

    @staticmethod
    def get_epoch_time_second():
        return time.time()

    @staticmethod
    def measure_execution_time(fn):
        time_start = Time.get_epoch_time_second()
        fn()
        elapsed_time = Time.get_epoch_time_second() - time_start
        return elapsed_time

    @staticmethod
    def get_date():
        now = datetime.datetime.now()
        date = now.strftime("%Y/%m/%d")
        return date

    @staticmethod
    def get_year_month():
        now = datetime.datetime.now()
        year_month = now.strftime("%Y/%m")
        return year_month
