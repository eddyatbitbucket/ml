import numpy as np


def slice_3d(tensor_3d, x_range, y_range, z_range):
    x_dim, y_dim, z_dim = len(x_range), len(y_range), len(z_range)
    sliced_list = list()

    for x in x_range:
        for y in y_range:
            for z in z_range:
                sliced_list.append(tensor_3d[x][y][z])
    result_tensor = np.array(sliced_list).reshape(x_dim, y_dim, z_dim)

    return result_tensor


def im2col(input_data, filter_h, filter_w, stride=1, pad=0):
    """

    Parameters
    ----------
    input_data : (データ数, チャンネル, 高さ, 幅)の4次元配列からなる入力データ
    filter_h : フィルターの高さ
    filter_w : フィルターの幅
    stride : ストライド
    pad : パディング

    Returns
    -------
    col : 2次元配列
    """
    N, C, H, W = input_data.shape
    out_h = (H + 2 * pad - filter_h) // stride + 1
    out_w = (W + 2 * pad - filter_w) // stride + 1

    img = np.pad(input_data, [(0, 0), (0, 0), (pad, pad), (pad, pad)], 'constant')
    col = np.zeros((N, C, filter_h, filter_w, out_h, out_w))

    for y in range(filter_h):
        y_max = y + stride * out_h
        for x in range(filter_w):
            x_max = x + stride * out_w
            col[:, :, y, x, :, :] = img[:, :, y:y_max:stride, x:x_max:stride]

    col = col.transpose(0, 4, 5, 1, 2, 3).reshape(N * out_h * out_w, -1)
    return col


def col2im(col, input_shape, filter_h, filter_w, stride=1, pad=0):
    """

    Parameters
    ----------
    col :
    input_shape : 入力データの形状（例：(10, 1, 28, 28)）
    filter_h :
    filter_w
    stride
    pad

    Returns
    -------

    """
    N, C, H, W = input_shape
    out_h = (H + 2 * pad - filter_h) // stride + 1
    out_w = (W + 2 * pad - filter_w) // stride + 1
    col = col.reshape(N, out_h, out_w, C, filter_h, filter_w).transpose(0, 3, 4, 5, 1, 2)

    img = np.zeros((N, C, H + 2 * pad + stride - 1, W + 2 * pad + stride - 1))
    for y in range(filter_h):
        y_max = y + stride * out_h
        for x in range(filter_w):
            x_max = x + stride * out_w
            img[:, :, y:y_max:stride, x:x_max:stride] += col[:, :, y, x, :, :]

    return img[:, :, pad:H + pad, pad:W + pad]


class Im2col3D:

    @staticmethod
    def compress(input_data, FD, FH, FW, stride=1, pad=0, use_cache=False, index_cache=None):
        """
        filter走過的路徑涵蓋的元素，壓縮拉直後輸出
        :param input_data:
        :param FD: filter depth (z)
        :param FH: filter height (y)
        :param FW: filter width (x)
        :param stride: 跨距
        :param pad: 邊緣擴充
        :param use_cache: 使用快取加速
        :param index_cache: 快取的index
        :return: 拉直後的tensor
        """
        # padding
        img = np.pad(input_data,
                     [(0, 0), (0, 0), (pad, pad), (pad, pad), (pad, pad)],
                     'constant')

        _masks = index_cache

        if use_cache and index_cache is not None:
            pass
        else:
            # print('產生Im2col3D快取中...')
            # _masks = Im2col3D.get_indexes(img, FD, FH, FW, stride)
            #
            # col_result = []
            # for _mask in _masks:
            #     col_result.append(np.take(img, _mask))
            # col_result = np.array(col_result)
            # 輸入tensor的形狀
            N, C, D, H, W = input_data.shape

            # 輸出tensor的形狀
            out_d = (D + 2 * pad - FD) // stride + 1
            out_h = (H + 2 * pad - FH) // stride + 1
            out_w = (W + 2 * pad - FW) // stride + 1

            col = np.zeros((N, C, FD, FH, FW, out_d, out_h, out_w))
            # strides =
            # sliding_boxes = np.lib.stride_tricks.as_strided()

            for z in range(FD):
                z_max = z + stride * out_d
                for y in range(FH):
                    y_max = y + stride * out_h
                    for x in range(FW):
                        x_max = x + stride * out_w
                        # index_cache[z, y, x] = [z, z_max, y, y_max, x, x_max, stride]
                        # print(f'z:{z}, y:{y}, x:{x}')
                        col[:, :, z, y, x, :, :, :] = \
                            img[:, :, z:z_max:stride, y:y_max:stride, x:x_max:stride]

                        # print(col.shape)

        col_t = col.transpose(0, 5, 6, 7, 1, 2, 3, 4)
        col_c = col_t.reshape(N * out_d * out_h * out_w, -1)
        col_c = col_c.astype(int)
        # assert np.array_equal(col_c, col_result)
        return col_c, _masks

    @staticmethod
    def get_indexes(cubes, FD, FH, FW, stride=1):
        """

        :param cubes: (N,C,D,H,W), N:num, C:color channel, D:depth, H:height, W:width
        :param FD: filter depth
        :param FH: filter height
        :param FW: filter width
        :param stride: 跨距
        :return:
        """
        # _indexes = dict()

        _masks = []
        N, C, D, H, W = cubes.shape
        for n in range(N):
            for c in range(C):
                for d in range(int((FD - 1) / 2), int(D - (FD - 1) / 2), stride):
                    for h in range(int((FH - 1) / 2), int(H - (FH - 1) / 2), stride):
                        for w in range(int((FW - 1) / 2), int(W - (FW - 1) / 2), stride):
                            # _indexes[(n, c, d, h, w)] = []
                            print(f'N:{n},C:{c}, D:{d}, H:{h}, W:{w}')
                            _indexes = []
                            for fd in range(FD):
                                for fh in range(FH):
                                    for fw in range(FW):
                                        _indexes.append((d - 1) * H * W + (h - 1) * W + (w - 1) +
                                                        fw + fh * W + fd * H * W)
                            _masks.append(_indexes)

        return np.array(_masks)

    @staticmethod
    def restore(input_shape, dcol_c, FD, FH, FW, stride=1, pad=0):
        N, C, D, H, W = input_shape
        out_d = (D + 2 * pad - FD) // stride + 1
        out_h = (H + 2 * pad - FH) // stride + 1
        out_w = (W + 2 * pad - FW) // stride + 1

        dcol_r = dcol_c.reshape(N, out_d, out_h, out_w,
                                C, FD, FH, FW)
        dcol = dcol_r.transpose(0, 4, 5, 6, 7, 1, 2, 3)

        img = np.zeros((N, C,
                        D + 2 * pad + stride - 1,
                        H + 2 * pad + stride - 1,
                        W + 2 * pad + stride - 1))

        for z in range(FD):
            z_max = z + stride * out_d
            for y in range(FH):
                y_max = y + stride * out_h
                for x in range(FW):
                    x_max = x + stride * out_w
                    dcol_temp = dcol[:, :, z, y, x, :, :, :]
                    img[:, :, z:z_max:stride, y:y_max:stride, x:x_max:stride] \
                        += dcol_temp

        return img[:, :, pad:D + pad, pad:H + pad, pad:W + pad]


if __name__ == '__main__':
    # v = \
    #     [
    #         [
    #             [
    #                 [1111, 1211, 1311, 1411],
    #                 [1121, 1221, 1321, 1421],
    #                 [1131, 1231, 1331, 1431]],
    #             [
    #                 [1112, 1212, 1312, 1412],
    #                 [1122, 1222, 1322, 1422],
    #                 [1132, 1232, 1332, 1432]],
    #             [
    #                 [1113, 1213, 1313, 1413],
    #                 [1123, 1223, 1323, 1423],
    #                 [1133, 1233, 1333, 1433]]
    #         ],
    #         [
    #             [
    #                 [2111, 2211, 2311, 2411],
    #                 [2121, 2221, 2321, 2421],
    #                 [2131, 2231, 2331, 2431]],
    #             [
    #                 [2112, 2212, 2312, 2412],
    #                 [2122, 2222, 2322, 2422],
    #                 [2132, 2232, 2332, 2432]],
    #             [
    #                 [2113, 2213, 2313, 2413],
    #                 [2123, 2223, 2323, 2423],
    #                 [2133, 2233, 2333, 2433]]
    #         ]
    #     ]
    # v = np.array(v)
    # v0 = np.array([v, v, v, v])

    # _col, cache = Im2col3D.compress(v0, 3, 3, 3, stride=1, pad=1)
    # print(_col)
    vv = [[[
        [
            [111, 112, 113, 114],
            [121, 122, 123, 124],
            [131, 132, 133, 134],
            [141, 142, 143, 144],
        ],
        [
            [211, 212, 213, 214],
            [221, 222, 223, 224],
            [231, 232, 233, 234],
            [231, 232, 233, 234],
        ],
        [
            [311, 312, 313, 314],
            [321, 322, 323, 324],
            [331, 332, 333, 334],
            [331, 332, 333, 334],
        ],
        [
            [411, 412, 413, 414],
            [421, 422, 423, 424],
            [431, 432, 433, 434],
            [431, 432, 433, 434],
        ]
    ]]]

    vv = np.array(vv)
    col_1 = Im2col3D.get_indexes(vv, 3, 3, 3, 1)

    # v1 = Im2col3D.restore(np.ones(_col.shape), 1, 3, 3, 3, stride=1, pad=1)
    # print(v1)
