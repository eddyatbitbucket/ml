from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout, Flatten
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.datasets import mnist
import numpy as np


def one_hot(t):
    t_one_hot = np.zeros((t.shape[0], 10))
    for i in range(t.shape[0]):
        t_one_hot[i][t[i]] = 1
    return t_one_hot


(x_train, t_train), (x_test, t_test) = mnist.load_data()
x_train=x_train.reshape((x_train.shape[0], 28,28,1))
x_test=x_test.reshape((x_test.shape[0], 28,28,1))
x_train=x_train/255
x_test=x_test/255

x_train = x_train.astype('float16')
x_test = x_test.astype('float16')

t_train = one_hot(t_train)
t_test = one_hot(t_test)

m = Sequential()
m.add(Conv2D(32, (3, 3), input_shape=(28,28,1)))
m.add(Activation('relu'))
m.add(Conv2D(16, (3, 3)))
m.add(Activation('relu'))
m.add(Conv2D(8, (3, 3)))
m.add(Activation('relu'))
m.add(Activation('relu'))
m.add(Conv2D(4, (3, 3)))
m.add(Activation('relu'))
m.add(Flatten())
m.add(Dense(10))
m.add(Activation('softmax'))

m.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])
m.fit(x_train, t_train, batch_size=100, epochs=1000000)
