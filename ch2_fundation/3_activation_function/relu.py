import numpy as np
import matplotlib.pyplot as plt
import activation_function


X = np.arange(-5, 5, step=0.1)
Y = activation_function.relu(X)

plt.plot(X, Y)
plt.show()
