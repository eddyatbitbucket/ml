import numpy as np
import matplotlib.pyplot as plt
import activation_function

X = np.arange(-10, 10, step=0.1)
Y = activation_function.sigmoid(X)

plt.plot(X, Y)
plt.show()
