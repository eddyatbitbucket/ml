import timeit

import numpy as np
from tensor import slice_3d

# 產生3d tensor
tensor = np.zeros((3, 3, 3))

for i in range(3):
    for j in range(3):
        tensor[i][j] = np.arange(0, 3) + 100 * i + 10 * j

print(tensor)

# slice
sliced_tensor = slice_3d(tensor, range(1, 3), range(1, 3), range(1, 3))
print('sliced tensor:\n', sliced_tensor)

# 測試slice_3d效能
t = timeit.Timer(stmt='slice_3d(tensor, range(1, 3), range(1, 3), range(1, 3))',
                 setup='from __main__ import  tensor, slice_3d')
print('1000,000次:', t.timeit(1000000))
