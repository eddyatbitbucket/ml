import numpy as np
import activation_function as h
import multiclassification_function


X = np.array([1, 2])
print("X:", X)
print(X.shape)  # 1x2

W = np.array([[1, 3, 5],
              [2, 4, 6]])
print('W:', W)
print(W.shape)  # 2x3

B = np.array([1, 1, 1])  # 1x3

Y = np.dot(X, W)+B
print("Y:", Y)
print(Y.shape)  # 1x3


Z=h.sigmoid(Y)
print(Z)