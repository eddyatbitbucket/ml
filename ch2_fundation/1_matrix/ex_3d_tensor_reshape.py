import numpy as np

# 產生3x3x3的張量
tensor = np.zeros((3, 3, 3))

for i in range(3):
    for j in range(3):
        tensor[i][j] = np.arange(3) + 100 * i + 10 * j

print('original:\n', tensor)

# 攤平成一維
tensor_1d = tensor.reshape(-1)
print('1d:\n', tensor_1d)

# reshape為原來的形狀
tensor_3d = tensor.reshape(3, 3, 3)
print('3d:\n', tensor_3d)
