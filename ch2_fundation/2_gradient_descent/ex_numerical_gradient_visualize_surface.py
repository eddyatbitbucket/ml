import numpy as np
from gradient_descent import numerical_gradient
import matplotlib.pyplot as plt
from matplotlib import cm

# 下面這行不可刪除不然會報錯：ValueError: Unknown projection '3d'
from mpl_toolkits.mplot3d import Axes3D


def circle(x):
    return np.sum(x ** 2, axis=0)


def complex_fx(x):
    return np.sin(np.sum(x ** 2, axis=0)) + 2


# f=circle
f = complex_fx

# 產生資料==================
x0 = np.arange(-2, 2.25, 0.25)
x1 = np.arange(-2, 2.25, 0.25)
X, Y = np.meshgrid(x0, x1)
Xf = X.flatten()
Yf = Y.flatten()
xy = np.array([Xf, Yf])
grad = numerical_gradient(f, xy)

# 2d=======================
fig = plt.figure(figsize=plt.figaspect(1.6))
fig.add_subplot(2, 1, 1)
plt.quiver(Xf, Yf, -grad[0], -grad[1], angles="xy", color="#666666")
plt.plot()

# 3d=======================
fig.add_subplot(2, 1, 2, projection='3d')
Z = f(np.array([X, Y]))
ax = fig.gca(projection='3d')

# 曲面
surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, linewidth=0, cmap=cm.get_cmap('coolwarm'))

# 等高線 zdir='z'投影到z
ax.contourf(X, Y, Z, zdir='z', offset=0, cmap=plt.get_cmap('coolwarm'))

plt.show()
