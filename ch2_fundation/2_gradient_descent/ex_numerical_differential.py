import numpy as np
import matplotlib.pylab as plt
import gradient_descent


def function1(x):
    return 0.1 * x ** 2 + 0.5 * x


X = np.arange(0, 20, 0.1)
Y = function1(X)
plt.plot(X, Y)

x1 = 10
a = gradient_descent.numerical_diff(function1, x1)
b = function1(x1) - x1 * a
Y = X * a + b
plt.plot(X, Y)

plt.show()
