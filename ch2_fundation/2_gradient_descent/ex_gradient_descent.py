import numpy as np
from gradient_descent import numerical_gradient
from gradient_descent import gradient_descent_with_history
import matplotlib.pyplot as plt
from matplotlib import cm

# 下面這行不可刪除不然會報錯：ValueError: Unknown projection '3d'
from mpl_toolkits.mplot3d import Axes3D


def circle(x):
    return np.sum(x ** 2, axis=0)


def complex_fx(x):
    return np.sin(np.sum(x ** 2, axis=0)) + 2


f=circle
# f = complex_fx

# 產生資料==================
x0 = np.arange(-4, 4.25, 0.25)
x1 = np.arange(-4, 4.25, 0.25)
X, Y = np.meshgrid(x0, x1)
Xf = X.flatten()
Yf = Y.flatten()
xy = np.array([Xf, Yf])
grad = numerical_gradient(f, xy)

# 2d=======================
fig = plt.figure()
plt.quiver(Xf, Yf, -grad[0], -grad[1], angles="xy", color="#666666")
plt.plot()


def function_2(x):
    return x[0] ** 2 + x[1] ** 2


init_x = np.array([-1.5, 0.5])
lr = 0.05
step_num = 2000
x, x_history = gradient_descent_with_history(f, init_x, learning_rate=lr, step_num=step_num)

plt.plot(x_history[:, 0], x_history[:, 1], 'o')
plt.show()
