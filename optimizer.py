# coding: utf-8
import numpy as np


class SGD:

    def __init__(self, learning_rate=0.01):
        self.lr = learning_rate

    def update(self, layer):
        layer.W -= self.lr * layer.dW
        layer.B -= self.lr * layer.dB


class Adam:
    """Adam paper (http://arxiv.org/abs/1412.6980v8)"""

    def __init__(self, learning_rate=0.001, beta1=0.9, beta2=0.999, initial_iter=0):
        self.lr = learning_rate
        self.beta1 = beta1
        self.beta2 = beta2
        self.iter = initial_iter
        self.m = {}
        self.v = {}
        self.mb = {}
        self.vb = {}

    def update(self, layer):
        k = layer.id
        if k not in self.m.keys():
            self.m[k] = np.zeros_like(layer.W)
            self.v[k] = np.zeros_like(layer.W)

            self.mb[k] = np.zeros_like(layer.B)
            self.vb[k] = np.zeros_like(layer.B)

        self.iter += 1

        lr_t = self.lr * np.sqrt(1.0 - self.beta2 ** self.iter) / (1.0 - self.beta1 ** self.iter)

        self.m[k] += (1 - self.beta1) * (layer.dW - self.m[k])
        self.v[k] += (1 - self.beta2) * (layer.dW ** 2 - self.v[k])
        layer.W -= lr_t * self.m[k] / (np.sqrt(self.v[k]) + 1e-7)

        self.mb[k] += (1 - self.beta1) * (layer.dB - self.mb[k])
        self.vb[k] += (1 - self.beta2) * (layer.dB ** 2 - self.vb[k])
        layer.B -= lr_t * self.mb[k] / (np.sqrt(self.vb[k]) + 1e-7)

#
# class Momentum:
#
#     def __init__(self, lr=0.01, momentum=0.9):
#         self.lr = lr
#         self.momentum = momentum
#         self.v = None
#
#     def update(self, params, grads):
#         if self.v is None:
#             self.v = {}
#             for key, val in params.items():
#                 self.v[key] = np.zeros_like(val)
#
#         for key in params.keys():
#             self.v[key] = self.momentum * self.v[key] - self.lr * grads[key]
#             params[key] += self.v[key]
#
#
# class Nesterov:
#     """Nesterov's Accelerated Gradient (http://arxiv.org/abs/1212.0901)"""
#
#     def __init__(self, lr=0.01, momentum=0.9):
#         self.lr = lr
#         self.momentum = momentum
#         self.v = None
#
#     def update(self, params, grads):
#         if self.v is None:
#             self.v = {}
#             for key, val in params.items():
#                 self.v[key] = np.zeros_like(val)
#
#         for key in params.keys():
#             self.v[key] *= self.momentum
#             self.v[key] -= self.lr * grads[key]
#             params[key] += self.momentum * self.momentum * self.v[key]
#             params[key] -= (1 + self.momentum) * self.lr * grads[key]
#
#
# class AdaGrad:
#     """AdaGrad"""
#
#     def __init__(self, lr=0.01):
#         self.lr = lr
#         self.h = None
#
#     def update(self, params, grads):
#         if self.h is None:
#             self.h = {}
#             for key, val in params.items():
#                 self.h[key] = np.zeros_like(val)
#
#         for key in params.keys():
#             self.h[key] += grads[key] * grads[key]
#             params[key] -= self.lr * grads[key] / (np.sqrt(self.h[key]) + 1e-7)
#
#
# class RMSprop:
#
#     def __init__(self, lr=0.01, decay_rate=0.99):
#         self.lr = lr
#         self.decay_rate = decay_rate
#         self.h = None
#
#     def update(self, params, grads):
#         if self.h is None:
#             self.h = {}
#             for key, val in params.items():
#                 self.h[key] = np.zeros_like(val)
#
#         for key in params.keys():
#             self.h[key] *= self.decay_rate
#             self.h[key] += (1 - self.decay_rate) * grads[key] * grads[key]
#             params[key] -= self.lr * grads[key] / (np.sqrt(self.h[key]) + 1e-7)
