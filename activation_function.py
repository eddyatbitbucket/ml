import numpy as np


def relu(X):
    """Rectified Linear Unit
    激發函數
    但與sigmoid不同處在於輸出數值沒有上限
    小於0時輸出0
    大於0時輸出x"""
    result = np.maximum(0, X)
    return result


def sigmoid(X):
    """激發函數
    通常用於二類別分類
    將輸出數值限制在0與1之間"""

    return 1 / (1 + np.exp(-X))


def softmax(X):
    """把array數值化為 0~1 的比率,並且總和為1
    通常應用於多類別分類
    其數值可被解讀為機率"""
    ex = np.exp(X)
    if ex.shape[1] == 1:
        s = np.sum(ex, axis=0)
    else:
        s = np.sum(ex, axis=1)

    result = (ex.T / s).T
    return result


def tanh(X):
    """tanh(x) = sinh(x)/cosh(x)"""
    result = np.sinh(X) / np.cosh(X)

    return result
