import cProfile
import pstats
import neural_network
from dataset.mnist import load_mnist
import gradient_descend_optimizer as gdo

def runner(nn):
    nn.learn(x_train, t_train,
             batch_size=100, iter_num=1000,
             plot_freq=100, save_as='pure_cnn.pkl')
    pass

# ===============================================================
(x_train, t_train), (x_test, t_test) = \
    load_mnist(normalize=True, flatten=False, one_hot_label=True)

# layer setting
conv0 = neural_network.Convolution(kernel_num=10, filter_size=25,
                                   pad=0, stride=1)
relu0 = neural_network.Relu()
pool0 = neural_network.Pooling(pool_h=4, pool_w=4, stride=2)

flatten = neural_network.Flatten()
last_layer = neural_network.SoftmaxWithLoss()

layers = [conv0, relu0, pool0,
          flatten,
          last_layer]

nn = neural_network.NeuralNetwork(layers, optimizer=gdo.Adam())

# ================================================================
cProfile.run("runner(nn)",
             'cprofile_report')

p = pstats.Stats('cprofile_report')
p.strip_dirs().sort_stats('tottime').print_stats(10)
p.strip_dirs().sort_stats('cumtime').print_stats(10)
