from nltk.tokenize import word_tokenize
import numpy as np
import nlp.dataset.ptb as ptb
from nlp.common.util import convert_one_hot, create_contexts_target, preprocess, most_similar
from nlp.common.layers import MatMul, SoftmaxWithLoss
from nlp.common.optimizer import SGD, Adam
from nlp.common.trainer import Trainer


class SimpleCBOW:
    def __init__(self, vocabulary_size, hidden_size):
        W_in = 0.01 * np.random.randn(vocabulary_size, hidden_size).astype('f')
        W_out = 0.01 * np.random.randn(hidden_size, vocabulary_size).astype('f')

        self.in_layer0 = MatMul(W_in)
        self.in_layer1 = MatMul(W_in)
        self.out_layer = MatMul(W_out)
        self.loss_layer = SoftmaxWithLoss()

        layers = [self.in_layer0, self.in_layer1, self.out_layer]
        self.params, self.grads = [], []
        for layer in layers:
            self.params += layer.params
            self.grads += layer.grads
        self.word_vectors = W_in

    def forward(self, contexts, target):
        h0 = self.in_layer0.forward(contexts[:, 0])
        h1 = self.in_layer1.forward(contexts[:, 1])
        h = (h0 + h1) * 0.5
        y = self.out_layer.forward(h)
        loss = self.loss_layer.forward(y, target)
        return loss

    def backward(self, dout=1):
        ds = self.loss_layer.backward(dout)
        da = self.out_layer.backward(ds)
        da *= 0.5

        self.in_layer1.backward(da)
        self.in_layer0.backward(da)


window_size = 1
hidden_size = 2
batch_size = 2
max_epoch = 5000

text = ''''
john gives you flowers. 
linda gives you flowers. 
luke gives me flowers.
david gives me flowers. 
'''


# corpus, word_to_id, id_to_word = ptb.load_data('valid')
corpus, word_to_id, id_to_word = preprocess(text)
print(word_to_id)

contexts, target = create_contexts_target(corpus, window_size)

vocabulary_size = len(word_to_id)
print(vocabulary_size)
target_one_hot = convert_one_hot(target, vocabulary_size)
contexts_one_hot = convert_one_hot(contexts, vocabulary_size)

model = SimpleCBOW(vocabulary_size, hidden_size)
optimizer = Adam()
trainer = Trainer(model, optimizer)

trainer.fit(contexts_one_hot, target_one_hot, max_epoch, batch_size)
trainer.plot()

for word_id, word in id_to_word.items():
    most_similar(word, word_to_id, id_to_word, model.word_vectors, top=3)
