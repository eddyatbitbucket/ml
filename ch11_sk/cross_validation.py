import numpy as np
import sklearn.datasets
import sklearn.model_selection
import sklearn.svm

# %% 基本train, test
iris = sklearn.datasets.load_iris()
X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(iris.data, iris.target,
                                                                            test_size=0.4, random_state=0)
svm_classifier = sklearn.svm.SVC(kernel='linear', C=1).fit(X_train, y_train)
score = svm_classifier.score(X_test, y_test)

# %% 10 fold cross validation
svm_classifier = sklearn.svm.SVC(kernel='linear', C=1)
scores = sklearn.model_selection.cross_val_score(svm_classifier, iris.data, iris.target, cv=10)

mean = scores.mean()
std = scores.std()

# %% 使用Kfold *****
import sklearn.model_selection

X = np.array(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'])
kf = sklearn.model_selection.KFold(n_splits=3, shuffle=True)

for train, test in kf.split(X):
    print(train, test)
    trainset = X[train]
    print(trainset)
