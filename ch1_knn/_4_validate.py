import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sklearn.neighbors
import sklearn.model_selection
import sklearn.metrics
import sklearn.preprocessing
import ch1_knn._2_normalize as normalize

df = pd.read_csv('./initial_d_data.csv')

X = df[['gender', 'age', 'cars', 'model_cars']]
Y = df['fan_grade']

# 正規化
X_scaled = normalize.normalize(X)

epoch = 1000
sum_accuracy = 0
for _ in range(epoch):
    X_train, X_test, Y_train, Y_test = \
        sklearn.model_selection.train_test_split(X_scaled, Y, shuffle=True, test_size=3)

    knn = sklearn.neighbors.KNeighborsClassifier(n_neighbors=3)
    knn.fit(X_train, Y_train)
    Y_predict = knn.predict(X_test)
    accuracy = sklearn.metrics.accuracy_score(Y_test, Y_predict)
    sum_accuracy += accuracy

print('mean accuracy:', sum_accuracy / epoch)
