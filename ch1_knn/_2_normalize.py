import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sklearn.neighbors
import sklearn


def normalize(df):
    min = np.min(df)
    max = np.max(df)
    return (df - min) / (max - min)


df = pd.read_csv('./initial_d_data.csv')

X_train = df.loc[:][['gender', 'age', 'cars', 'model_cars']]

print(normalize(X_train))
