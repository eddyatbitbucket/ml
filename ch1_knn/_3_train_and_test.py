import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sklearn.neighbors
import sklearn


df = pd.read_csv('./initial_d_data.csv')

X_train = df.loc[:][['gender', 'age', 'cars', 'model_cars']]
Y_train = df.loc[:]['fan_grade']

knn = sklearn.neighbors.KNeighborsClassifier(n_neighbors=3)
knn.fit(X_train, Y_train)

y = knn.predict([[1, 60, 1, 5]])
print(y)
