import os
import pickle
import zipfile
import matplotlib.pyplot as plt
import numpy as np

size_limit = 2 ** 31 - 1


def save(data, file_path, compress=False):
    print(f'正在儲存 {file_path}...')
    try:
        bytes_out = pickle.dumps(data)

        if compress:
            with zipfile.ZipFile(file_path, 'w') as zf:
                zf.writestr(file_path, bytes_out, compress_type=zipfile.ZIP_DEFLATED)
        else:
            with open(file_path, 'wb') as f:
                for i in range(0, len(bytes_out), size_limit):
                    f.write(bytes_out[i:i + size_limit])

        result = True
        print(f'已儲存 {file_path}')
    except Exception as e:
        print('儲存失敗', e)
        result = False

    return result


def load(file_path):
    print(f'載入中 {file_path}...')

    try:
        if os.path.exists(file_path):
            if zipfile.is_zipfile(file_path):
                with zipfile.ZipFile(file_path) as zf:
                    bytes_in = zf.read(file_path)
            else:
                file_size = os.path.getsize(file_path)
                bytes_in = bytearray(0)

                with open(file_path, 'rb') as f:
                    for _ in range(0, file_size, size_limit):
                        bytes_in += f.read(size_limit)
            data = pickle.loads(bytes_in)
            print(f'已載入 {file_path}')
        else:
            data = None
            print(f'找不到 {file_path}')
    except Exception as e:
        print('error while loading data', e)
        data = None

    return data


def save_img(path, data):
    plt.imsave(path, data, format="png", cmap="gray", vmin=0, vmax=255)
