# -*- coding: utf-8 -*-
import os
import sys

import storage
import utility

curPath = os.path.abspath(os.path.dirname(__file__))
project_root = os.path.dirname(curPath)
sys.path.insert(0, project_root)
# 在sys.path還未設定之前，不能執行其他import，否則可能會有module not found exception


import numpy as np
from dataset.mnist import load_mnist
from neural_network import NeuralNetwork
from neural_network import Affine
from neural_network import Convolution
from neural_network import Pooling
from neural_network import Relu
from neural_network import Sigmoid
from neural_network import SoftmaxWithLoss
from neural_network import Flatten
from neural_network import Inflate
from neural_network import Tanh
from neural_network import MeanSquareLoss

(x_train, t_train), (x_test, t_test) = \
    load_mnist(normalize=True, flatten=False, one_hot_label=True)
x_train = x_train.reshape([-1, 28 * 28])

input_num = x_train.shape[1]

# encoder
encode_affine1 = Affine(100)
encode_relu1 = Relu()

encode_affine2 = Affine(10)
encode_relu2 = Relu()

encode_affine3 = Affine(2)
encode_sigmoid = Sigmoid()

encode_layers = [encode_affine1, encode_relu1,
                 encode_affine2, encode_relu2,
                 encode_affine3, encode_sigmoid
                 ]

decode_affine1 = Affine(10)
decode_relu1 = Relu()

decode_affine2 = Affine(100)
decode_relu2 = Relu()

decode_affine3 = Affine(28 * 28)
decode_tanh = Sigmoid()

last_layer = MeanSquareLoss()

decode_layers = [decode_affine1, decode_relu1,
                 decode_affine2, decode_relu2,
                 decode_affine3, decode_tanh,
                 last_layer
                 ]

nn = NeuralNetwork(encode_layers + decode_layers)

nn.learn(x_train, x_train, iter_num=10000, plot_freq=True, classification=False)

# storage.save(network.layers, 'auto_encoder.pkl')
